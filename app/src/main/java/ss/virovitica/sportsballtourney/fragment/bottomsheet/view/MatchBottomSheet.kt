package ss.virovitica.sportsballtourney.fragment.bottomsheet.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.view_goal.view.*
import kotlinx.android.synthetic.main.view_match_detail.*
import kotlinx.android.synthetic.main.view_match_detail.emptyView
import kotlinx.android.synthetic.main.view_match_detail.progressView
import kotlinx.android.synthetic.main.view_match_preview.nameTeamA
import kotlinx.android.synthetic.main.view_match_preview.nameTeamB
import kotlinx.android.synthetic.main.view_match_preview.scoreTeamA
import kotlinx.android.synthetic.main.view_match_preview.scoreTeamB
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.dialog.AddGoalDialog
import ss.virovitica.sportsballtourney.dialog.AddGoalDialog.Companion.ADD_GOAL_DIALOG
import ss.virovitica.sportsballtourney.dialog.FinalizeResultsDialog
import ss.virovitica.sportsballtourney.dialog.FinalizeResultsDialog.Companion.FINALIZE_RESULTS_DIALOG
import ss.virovitica.sportsballtourney.fragment.bottomsheet.presenter.IMatchSheetPresenter
import ss.virovitica.sportsballtourney.model.Goal
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.utility.getScreenHeight

class MatchBottomSheet(private val matchID: String, private val tieAllowed: Boolean, private val authorizedManager: Boolean):
    BottomSheetDialogFragment(), IMatchSheetView, AddGoalDialog.Listener, FinalizeResultsDialog.Listener {

    companion object {
        const val MATCH_BOTTOM_SHEET = "ss.virovitica.sportsballtourney.fragment.bottomsheet.view.MatchBottomSheet"
    }

    //members
    override val ctx: Context
        get() = context!!
    private val presenter: IMatchSheetPresenter by inject{ parametersOf(this) }
    private lateinit var match: Match

    //functions

    private fun makeGoalView(goal: Goal, players: Map<String, Player>): View {
        val goalView = layoutInflater.inflate(R.layout.view_goal, null, false)
        goalView.goalValueText.text = goal.value.toString()
        val scoringPlayer = players[goal.scoringPlayerID]
        if(scoringPlayer != null) {
            goalView.goalPlayerText.text =
                if(scoringPlayer.number != null) getString(R.string.generic_player, scoringPlayer.number, scoringPlayer.name)
                else scoringPlayer.name
        }
        else goalView.playerLayout.visibility = GONE
        goalView.goalTimeText.text = ctx.resources.getQuantityString(R.plurals.generic_minutes_number, goal.time, goal.time)
        return goalView
    }

    //***BottomSheetDialogFragment***//

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_match_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.fetchMatch(matchID)
        addResultsButton.setOnClickListener { //show a dialog to add a point
            val teamA = presenter.teamA
            val teamB = presenter.teamB
            if(teamA != null && teamB != null) {
                AddGoalDialog(this, teamA, teamB, presenter.playersMap).show(activity!!.supportFragmentManager, ADD_GOAL_DIALOG)
            }
        }
        if(!tieAllowed) finalizeResultsButton.isEnabled = false //this button will be enabled if the match is not a tie
        finalizeResultsButton.setOnClickListener { //show dialog to ask for confirmation
            FinalizeResultsDialog(this).show(activity!!.supportFragmentManager, FINALIZE_RESULTS_DIALOG)
        }
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    //***IMatchSheetView***//

    override fun setMatch(match: Match) {
        //fill in total points
        this.match = match
        scoreTeamA?.text = match.scoreTeamA.toString()
        scoreTeamB?.text = match.scoreTeamB.toString()
        //enable or disable the "finalize results" button depending on scores
        finalizeResultsButton?.isEnabled = (tieAllowed || match.scoreTeamA != match.scoreTeamB)
        //if there are no goals, hide the view that would hold them and show the no goals message
        if(match.getGoalsTeamA().isEmpty() && match.getGoalsTeamB().isEmpty()) {
            pointsDetails?.visibility = GONE
            emptyView?.visibility = VISIBLE
        }
        else {
            pointsDetails?.visibility = VISIBLE
            emptyView?.visibility = GONE
        }
        //if the match is finished, hide the buttons to add or finish results
        if(!authorizedManager || match.finished) resultButtonBar?.visibility = GONE
        else resultButtonBar?.visibility = VISIBLE
    }

    override fun setTeams(teamA: Team?, teamB: Team?) {
        //fill in team names
        nameTeamA?.text = teamA?.name
        nameTeamB?.text = teamB?.name
    }

    override fun setPlayers(players: Map<String, Player>) {
        if(pointsTeamA == null || pointsTeamB == null) return
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        pointDetailsScroll?.visibility = VISIBLE
        //fill in individual points (since we now have data on which players scored them)
        pointsTeamA?.removeAllViews()
        pointsTeamB?.removeAllViews()
        for (goal in match.getGoalsTeamA()) {
            pointsTeamA?.addView(makeGoalView(goal, players))
        }
        for (goal in match.getGoalsTeamB()) {
            pointsTeamB?.addView(makeGoalView(goal, players))
        }
        pointDetailsScroll?.post {
            val maxHeight = (getScreenHeight() * 2) / 3
            val currentHeight = pointDetailsScroll.height
            if(currentHeight > maxHeight) {
                val params = pointDetailsScroll.layoutParams
                params.height = maxHeight
                pointDetailsScroll.layoutParams = params
            }
        }
    }

    //***AddGoalDialog.Listener***//

    override fun onSaveGoal(goal: Goal, byTeamA: Boolean) {
        presenter.addGoal(goal, byTeamA)
    }

    //***FinalizeResultsDialog.Listener***//

    override fun onFinalize() {
        presenter.finishMatch()
    }
}