package ss.virovitica.sportsballtourney.fragment.tournamentinfo.presenter

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.fragment.tournamentinfo.view.ITournamentInfoView
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.model.User
import java.util.*

class TournamentInfoPresenter(private val view: ITournamentInfoView, private val auth: FirebaseAuth, private val database: ILiveDatabase):
    ITournamentInfoPresenter, ILiveDatabase.Listener {

    //members
    override lateinit var tournament: Tournament
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private var isDeleting = false

    //functions

    private fun generateGroupMatches() {
        for(group in tournament.getGroups()) {
            for(i in 0 until group.getTeamIDs().size) { //team A
                val teamIdA = group.getTeamIDs()[i]
                for(j in i+1 until group.getTeamIDs().size) { //team B
                    val teamIdB = group.getTeamIDs()[j]
                    val match = Match(teamIdA, teamIdB)
                    group.addMatch(match.id)
                    database.addMatch(match)
                }
            }
        }
    }

    //***ITournamentInfoPresenter***//

    override fun fetchTournament(tournamentID: String) { database.getTournament(this, tournamentID) }

    override fun setVisibility() {
        val user = auth.currentUser
        if(user == null) { //guest
            view.setGuestVisibility()
        }
        else { //logged in
            var isManager = false
            for (manager in tournament.managers) {
                //scan the tournament's list of managing users for the current user
                if(manager.id == user.uid) {
                    isManager = true
                    break
                }
            }
            if(isManager) { //manager
                view.setOwnerVisibility()
            }
            else { //non-manager, but logged in
                view.setGeneralVisibility()
            }
        }
    }

    override fun deleteTournament() {
        isDeleting = true
        database.deleteTournament(tournament.id)
    }

    override fun fetchTeams(teamIDs: List<String>) {
        database.getTeams(this, teamIDs)
    }

    override fun updateManagingUsers(managingUsers: List<User>) {
        tournament.managers.clear()
        tournament.managers.addAll(managingUsers)
        database.updateTournament(tournament)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    override fun startTournament() {
        if(tournament.startTournament()) { //creates groups
            generateGroupMatches() //fills groups with their matches
            database.updateTournament(tournament)
        }
        else view.ctx.toast(R.string.start_failed)
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        this.tournament = tournament
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        setVisibility()
        view.fillViews(tournament)
    }

    override fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        view.addTeams(teams)
    }

    override fun dataError(messageID: Int) {
        if(isDeleting) {
            isDeleting = false
            return
        }
        Log.i("TournamentInfoPresenter", "dataError called")
    }
}