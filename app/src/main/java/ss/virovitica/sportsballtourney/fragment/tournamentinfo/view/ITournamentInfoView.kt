package ss.virovitica.sportsballtourney.fragment.tournamentinfo.view

import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface ITournamentInfoView: IView {
    fun setGuestVisibility()
    fun setOwnerVisibility()
    fun setGeneralVisibility()
    fun fillViews(tournament: Tournament)
    fun addTeams(teams: List<Team>)
}