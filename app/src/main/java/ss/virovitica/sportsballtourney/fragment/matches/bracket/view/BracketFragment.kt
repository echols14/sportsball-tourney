package ss.virovitica.sportsballtourney.fragment.matches.bracket.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_bracket.*
import kotlinx.android.synthetic.main.fragment_bracket.emptyView
import kotlinx.android.synthetic.main.fragment_bracket.progressView
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.adapter.BracketMatchesExpandableAdapter
import ss.virovitica.sportsballtourney.fragment.bottomsheet.view.MatchBottomSheet
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.IBracketPresenter
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team

class BracketFragment: Fragment(), IBracketView {
    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.fragment.matches.bracket.view.BracketFragment.KEY_TOURNAMENT_ID"
    }

    //members
    override val ctx: Context
        get() = context!!
    private val presenter: IBracketPresenter by inject{ parametersOf(this) }
    private val adapter = BracketMatchesExpandableAdapter()
    private var tournamentID = ""
    private var firstOpen = true

    //functions

    //***Fragment***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //get info from the arguments
        tournamentID = arguments?.getString(KEY_TOURNAMENT_ID) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bracket, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bracketMatchesExpandableList.setAdapter(adapter)
        bracketMatchesExpandableList.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            //open bottom sheet for the selected match
            val matchID = adapter.getChild(groupPosition, childPosition).id
            val bottomSheet = MatchBottomSheet(matchID, false, presenter.authorizedUser)
            bottomSheet.show(activity!!.supportFragmentManager, MatchBottomSheet.MATCH_BOTTOM_SHEET)
            true
        }
        //grab tournament from database
        if(tournamentID != "") presenter.fetchTournament(tournamentID)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    //***IBracketView***//

    override fun setTeams(teams: List<Team>) { adapter.setTeams(teams) }

    override fun setMatches(matches: List<Match>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(matches.isEmpty()) {
            bracketMatchesExpandableList?.visibility = GONE
            emptyView?.visibility = VISIBLE
        }
        else bracketMatchesExpandableList?.visibility = VISIBLE
        //put the data in
        adapter.setMatches(matches)
        if(firstOpen) {
            firstOpen = false
            for (i in 0 until adapter.groupCount) {
                val level = adapter.getGroup(i)
                if(level.isEmpty()) continue //if a level is empty, leave it collapsed
                //if it's the final or an unfinished level, expand it
                if(i == adapter.groupCount-1 || !adapter.isLevelFinished(i)) bracketMatchesExpandableList?.expandGroup(i)
            }
        }
    }
}