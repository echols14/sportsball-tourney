package ss.virovitica.sportsballtourney.fragment.matches.groups.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_groups.*
import kotlinx.android.synthetic.main.fragment_groups.emptyView
import kotlinx.android.synthetic.main.fragment_groups.progressView
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.adapter.GroupMatchesExpandableAdapter
import ss.virovitica.sportsballtourney.fragment.bottomsheet.view.MatchBottomSheet
import ss.virovitica.sportsballtourney.fragment.bottomsheet.view.MatchBottomSheet.Companion.MATCH_BOTTOM_SHEET
import ss.virovitica.sportsballtourney.fragment.matches.groups.presenter.IGroupsPresenter
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament

class GroupsFragment: Fragment(), IGroupsView {
    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.fragment.matches.groups.view.GroupsFragment.KEY_TOURNAMENT_ID"
    }

    //members
    override val ctx: Context
        get() = context!!
    private val presenter: IGroupsPresenter by inject{ parametersOf(this) }
    private val adapter = GroupMatchesExpandableAdapter()
    private var tournamentID: String = ""
    private var firstOpen = true

    //functions

    //***Fragment***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //get info from the arguments
        tournamentID = arguments?.getString(KEY_TOURNAMENT_ID) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_groups, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //set up the expandable list
        groupMatchesExpandableList.setAdapter(adapter)
        groupMatchesExpandableList.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            //open bottom sheet for the selected match
            val matchID = adapter.getChild(groupPosition, childPosition).id
            val bottomSheet = MatchBottomSheet(matchID, true, presenter.authorizedUser)
            bottomSheet.show(activity!!.supportFragmentManager, MATCH_BOTTOM_SHEET)
            true
        }
        //grab tournament from database
        if(tournamentID != "") presenter.fetchTournament(tournamentID)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    //***IGroupsView***//

    override fun setTournament(tournament: Tournament) { adapter.setGroups(tournament.getGroups()) }
    override fun setTeams(teams: List<Team>) { adapter.setTeams(teams) }
    override fun setMatches(matches: List<Match>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(matches.isEmpty()) {
            groupMatchesExpandableList?.visibility = GONE
            emptyView?.visibility = VISIBLE
        }
        else groupMatchesExpandableList?.visibility = VISIBLE
        //put the data in
        adapter.setMatches(matches)
        if(firstOpen) {
            firstOpen = false
            for (i in 0 until adapter.groupCount) {
                if(!adapter.getGroup(i).finished) groupMatchesExpandableList?.expandGroup(i)
            }
        }
    }
}