package ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.fragment.matches.bracket.view.IBracketView
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.generateNextBracketLevel
import java.util.*
import kotlin.collections.ArrayList

class BracketPresenter(private val view: IBracketView, private val database: ILiveDatabase, private val auth: FirebaseAuth): IBracketPresenter, ILiveDatabase.Listener {
    companion object {
        //if any of these values change, verify that BracketMatchesExpandableAdapter still functions properly
        const val LEVELS = 3
        const val QUARTER_FINAL_SIZE = 4
        const val QUARTER_FINAL_INDEX = 0
        const val SEMI_FINAL_SIZE = 2
        const val SEMI_FINAL_INDEX = 1
        const val FINAL_SIZE = 1
        const val FINAL_INDEX = 2

        const val BREAK_INDEX_1 = QUARTER_FINAL_SIZE
        const val BREAK_INDEX_2 = QUARTER_FINAL_SIZE + SEMI_FINAL_SIZE
        const val BREAK_INDEX_3 = QUARTER_FINAL_SIZE + SEMI_FINAL_SIZE + FINAL_SIZE
    }
    
    //members
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private lateinit var tournament: Tournament
    private var matches: List<Match> = ArrayList()
    override var authorizedUser = false
    
    //functions

    //private

    private fun checkBracketLevelCompletion() {
        //if the next level of the bracket cannot be made, we return
        //figure out what level we're currently on and grab it
        println("test")
        val recentLevel: List<Match> = when(matches.size) {
            BREAK_INDEX_1 -> matches.subList(0, BREAK_INDEX_1)
            BREAK_INDEX_2 -> matches.subList(BREAK_INDEX_1, BREAK_INDEX_2)
            BREAK_INDEX_3 -> matches.subList(BREAK_INDEX_2, BREAK_INDEX_3)
            else -> return
        }
        //check if this level is finished
        for(match in recentLevel) {
            if(!match.finished) return
        }
        //if it's the last level (size 1) we can't make another level
        if(recentLevel.size == 1) {
            tournament.finishTournament()
            database.updateTournament(tournament)
        }
        //create the next level if we've made it this far
        val nextLevel = generateNextBracketLevel(recentLevel)
        tournament.addMatches(nextLevel)
        database.addMatches(nextLevel)
        database.updateTournament(tournament)
    }

    //***IBracketPresenter***//

    override fun fetchTournament(tournamentID: String) {
        database.getTournament(this, tournamentID)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.tournament = tournament
        val currentUserID = auth.currentUser?.uid
        authorizedUser = false
        for(user in tournament.managers) {
            if(user.id == currentUserID) {
                authorizedUser = true
                break
            }
        }
        database.getTeams(this, tournament.teamIDs)
        database.getMatches(this, tournament.getBracketMatchIDs())
    }

    override fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        view.setTeams(teams)
    }

    override fun matchesUpdate(matches: List<Match>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.matches = matches
        view.setMatches(matches)
        checkBracketLevelCompletion()
    }

    override fun dataError(messageID: Int) {
        view.ctx.toast(messageID)
    }
}