package ss.virovitica.sportsballtourney.fragment.teams.presenter

interface ITeamsPresenter {
    fun fetchTournament(tournamentID: String)
    fun unsubscribe()
}