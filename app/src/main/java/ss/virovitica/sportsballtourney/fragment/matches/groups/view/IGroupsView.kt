package ss.virovitica.sportsballtourney.fragment.matches.groups.view

import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface IGroupsView: IView {
    fun setTournament(tournament: Tournament)
    fun setTeams(teams: List<Team>)
    fun setMatches(matches: List<Match>)
}