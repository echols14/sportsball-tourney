package ss.virovitica.sportsballtourney.fragment.bottomsheet.presenter

import ss.virovitica.sportsballtourney.model.Goal
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.model.Team

interface IMatchSheetPresenter {
    val teamA: Team?
    val teamB: Team?
    val playersMap: Map<String, Player>
    fun fetchMatch(matchID: String)
    fun addGoal(goal: Goal, byTeamA: Boolean)
    fun finishMatch()
    fun unsubscribe()
}