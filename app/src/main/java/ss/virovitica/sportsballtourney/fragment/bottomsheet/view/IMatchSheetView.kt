package ss.virovitica.sportsballtourney.fragment.bottomsheet.view

import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface IMatchSheetView: IView {
    fun setMatch(match: Match)
    fun setTeams(teamA: Team?, teamB: Team?)
    fun setPlayers(players: Map<String, Player>)
}