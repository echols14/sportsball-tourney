package ss.virovitica.sportsballtourney.fragment.teams.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_teams.*
import kotlinx.android.synthetic.main.fragment_teams.emptyView
import kotlinx.android.synthetic.main.fragment_teams.progressView
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.team.view.TeamActivity
import ss.virovitica.sportsballtourney.activity.team.view.TeamActivity.Companion.KEY_TEAM_ID
import ss.virovitica.sportsballtourney.adapter.TeamExpandableAdapter
import ss.virovitica.sportsballtourney.fragment.teams.presenter.ITeamsPresenter
import ss.virovitica.sportsballtourney.model.Group
import ss.virovitica.sportsballtourney.model.Team
import java.util.HashMap

class TeamsFragment: Fragment(), ITeamsView {
    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.fragment.teams.view.TeamsFragment.KEY_TOURNAMENT_ID"
    }

    //members
    override val ctx: Context
        get() = context!!
    private val presenter: ITeamsPresenter by inject{ parametersOf(this)}
    private val adapter = TeamExpandableAdapter()
    private var tournamentID = ""
    private var firstOpen = true

    //functions

    //***Fragment***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tournamentID = arguments?.getString(KEY_TOURNAMENT_ID) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_teams, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //set up the expandable list
        teamsExpandableList.setAdapter(adapter)
        teamsExpandableList.setOnChildClickListener { _, _, groupPosition, childPosition, _ ->
            val teamID = adapter.getChild(groupPosition, childPosition).id
            ctx.startActivity<TeamActivity>(TeamActivity.KEY_TOURNAMENT_ID to tournamentID, KEY_TEAM_ID to teamID)
            true
        }
        //grab tournament from database
        if(tournamentID != "") presenter.fetchTournament(tournamentID)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    //***ITeamsView***//

    override fun setGroups(groups: List<Group>) { adapter.setGroups(groups) }
    override fun setTeams(teams: List<Team>) { adapter.setTeams(teams) }
    override fun setStandings(groupStandings: Map<Int, List<String>>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(groupStandings.isEmpty()) {
            teamsExpandableList?.visibility = GONE
            emptyView?.visibility = VISIBLE
        }
        else teamsExpandableList?.visibility = VISIBLE
        //put the data in
        adapter.setStandings(groupStandings)
        if(firstOpen) {
            firstOpen = false
            for (i in 0 until adapter.groupCount) teamsExpandableList?.expandGroup(i)
        }
    }
    override fun setMetaPoints(metaPoints: HashMap<String, Int>) { adapter.setMetaPoints(metaPoints) }
    override fun setDifferencePoints(differencePoints: HashMap<String, Int>) {
        adapter.setDifferencePoints(differencePoints)
    }
}