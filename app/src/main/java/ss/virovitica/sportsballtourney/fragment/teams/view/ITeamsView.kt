package ss.virovitica.sportsballtourney.fragment.teams.view

import ss.virovitica.sportsballtourney.model.Group
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.utility.`interface`.IView
import java.util.HashMap

interface ITeamsView: IView {
    fun setGroups(groups: List<Group>)
    fun setTeams(teams: List<Team>)
    fun setStandings(groupStandings: Map<Int, List<String>>)
    fun setMetaPoints(metaPoints: HashMap<String, Int>)
    fun setDifferencePoints(differencePoints: HashMap<String, Int>)
}