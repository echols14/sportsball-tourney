package ss.virovitica.sportsballtourney.fragment.matches.groups.presenter

import ss.virovitica.sportsballtourney.model.Team

interface IGroupsPresenter {
    val teams: List<Team>
    val authorizedUser: Boolean
    fun fetchTournament(tournamentID: String)
    fun unsubscribe()
}