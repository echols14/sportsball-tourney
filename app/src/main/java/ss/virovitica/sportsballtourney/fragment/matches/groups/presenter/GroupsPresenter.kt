package ss.virovitica.sportsballtourney.fragment.matches.groups.presenter

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.fragment.matches.groups.view.IGroupsView
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.generateFirstLevelOfBracket
import java.util.*
import kotlin.collections.HashMap

class GroupsPresenter(private val view: IGroupsView, private val database: ILiveDatabase, private val auth: FirebaseAuth): IGroupsPresenter, ILiveDatabase.Listener {
    //members
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private lateinit var tournament: Tournament
    override var teams: List<Team> = LinkedList()
        private set
    private val matches: MutableMap<String, Match> = HashMap()
    override var authorizedUser = false

    //functions

    //private

    private fun checkGroupCompletion() {
        var update = false
        var allFinished = true
        if(tournament.getGroups().isEmpty()) allFinished = false
        for(group in tournament.getGroups()) {
            //figure out if its finished
            var finished = true
            for(matchID in group.getMatchIDs()) {
                val match = matches[matchID]
                if(match == null || !match.finished) {
                    finished = false
                    break
                }
            }
            //if it is, mark it so
            if(finished && !group.finished) {
                group.finish()
                update = true
            }
            if(!group.finished)  allFinished = false
        }
        if(allFinished && !tournament.bracketStarted) {
            tournament.startBracket()
            database.updateTournament(tournament)
            val firstLevelOfBracket = generateFirstLevelOfBracket(tournament, matches)
            tournament.addMatches(firstLevelOfBracket)
            database.addMatches(firstLevelOfBracket)
            update = true
        }
        if(update) database.updateTournament(tournament)
    }

    //***IGroupsPresenter***//

    override fun fetchTournament(tournamentID: String) {
        database.getTournament(this, tournamentID)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        val currentUserID = auth.currentUser?.uid
        authorizedUser = false
        for(user in tournament.managers) {
            if(user.id == currentUserID) {
                authorizedUser = true
                break
            }
        }
        this.tournament = tournament
        view.setTournament(tournament)
        database.getTeams(this, tournament.teamIDs)
        val allMatchIDs = LinkedList<String>()
        for(group in tournament.getGroups()) {
            allMatchIDs.addAll(group.getMatchIDs())
        }
        database.getMatches(this, allMatchIDs)
    }

    override fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        val allPlayerIDs: MutableList<String> = LinkedList()
        for(team in teams) {
            allPlayerIDs.addAll(team.playerIDs)
        }
        this.teams = teams
        view.setTeams(teams)
    }

    override fun matchesUpdate(matches: List<Match>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        for(match in matches) this.matches[match.id] = match
        view.setMatches(matches)
        checkGroupCompletion()
    }

    override fun dataError(messageID: Int) {
        view.ctx.toast(messageID)
    }
}