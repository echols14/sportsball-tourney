package ss.virovitica.sportsballtourney.fragment.matches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_matches.*
import ss.virovitica.sportsballtourney.R
import android.graphics.PorterDuff
import android.os.Build
import android.widget.Button
import androidx.annotation.ColorRes
import ss.virovitica.sportsballtourney.fragment.matches.bracket.view.BracketFragment
import ss.virovitica.sportsballtourney.fragment.matches.groups.view.GroupsFragment
import ss.virovitica.sportsballtourney.utility.switchToFragment

class MatchesFragment: Fragment() {
    companion object {
        const val KEY_GROUPS_FINISHED = "ss.virovitica.sportsballtourney.fragment.matches.MatchesFragment.KEY_GROUPS_FINISHED"
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.fragment.matches.MatchesFragment.KEY_TOURNAMENT_ID"
    }

    //members
    private var groupsShown = false
    private val groupsFragment = GroupsFragment()
    private val bracketFragment = BracketFragment()
    private var groupsFinished = false

    //functions

    private fun selectGroupsTab() {
        if(groupsShown) return
        groupsShown = true
        tintViewDrawable(groupsNavigation, R.color.colorSecondaryDark)
        tintViewDrawable(bracketNavigation, R.color.colorWhite)
        switchToFragment(activity!!.supportFragmentManager, R.id.innerFragmentHolder, groupsFragment)
    }

    private fun selectBracketTab() {
        if(!groupsShown) return
        groupsShown = false
        tintViewDrawable(groupsNavigation, R.color.colorWhite)
        tintViewDrawable(bracketNavigation, R.color.colorSecondaryDark)
        switchToFragment(activity!!.supportFragmentManager, R.id.innerFragmentHolder, bracketFragment)
    }

    private fun tintViewDrawable(view: Button, @ColorRes colorID: Int) {
        //get the appropriate color
        val tempActivity = activity
        val color = if(tempActivity != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            resources.getColor(colorID, tempActivity.theme)
        } else resources.getColor(colorID)
        //set the text and drawable to be that color
        view.setTextColor(color)
        for (drawable in view.compoundDrawables) {
            drawable?.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }

    //***Fragment***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //get info from the arguments
        groupsFinished = arguments?.getBoolean(KEY_GROUPS_FINISHED) ?: false
        val tournamentID = arguments?.getString(KEY_TOURNAMENT_ID)
        //forward tournament ID to child fragments
        if(tournamentID != null) {
            //Groups tab
            val groupsArgs = Bundle()
            groupsArgs.putString(GroupsFragment.KEY_TOURNAMENT_ID, tournamentID)
            groupsFragment.arguments = groupsArgs
            //Bracket tab
            val bracketArgs = Bundle()
            bracketArgs.putString(BracketFragment.KEY_TOURNAMENT_ID, tournamentID)
            bracketFragment.arguments = bracketArgs
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_matches, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //set up the custom bottom navigation
        groupsNavigation.setOnClickListener { selectGroupsTab() }
        bracketNavigation.setOnClickListener { selectBracketTab() }
        //select the first tab
        if(groupsFinished) {
            groupsShown = true
            selectBracketTab()
        }
        else selectGroupsTab()
    }
}