package ss.virovitica.sportsballtourney.fragment.matches.bracket.view

import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface IBracketView: IView {
    fun setTeams(teams: List<Team>)
    fun setMatches(matches: List<Match>)
}