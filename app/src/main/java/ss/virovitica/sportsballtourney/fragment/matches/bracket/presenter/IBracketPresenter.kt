package ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter

interface IBracketPresenter {
    val authorizedUser: Boolean
    fun fetchTournament(tournamentID: String)
    fun unsubscribe()
}