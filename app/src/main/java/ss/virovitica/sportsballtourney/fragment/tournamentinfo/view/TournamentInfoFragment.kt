package ss.virovitica.sportsballtourney.fragment.tournamentinfo.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_tournament_info.*
import kotlinx.android.synthetic.main.fragment_tournament_info.view.*
import kotlinx.android.synthetic.main.view_manager.view.*
import kotlinx.android.synthetic.main.view_team.view.*
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editplayers.view.EditPlayersActivity
import ss.virovitica.sportsballtourney.activity.editplayers.view.EditPlayersActivity.Companion.KEY_IS_OWNER
import ss.virovitica.sportsballtourney.activity.editplayers.view.EditPlayersActivity.Companion.KEY_TEAM_ID
import ss.virovitica.sportsballtourney.activity.editteams.view.EditTeamsActivity
import ss.virovitica.sportsballtourney.activity.main.view.IMainView
import ss.virovitica.sportsballtourney.activity.tournaments.view.TournamentsActivity
import ss.virovitica.sportsballtourney.dialog.DeleteTournamentDialog
import ss.virovitica.sportsballtourney.dialog.DeleteTournamentDialog.Companion.DELETE_TOURNAMENT_DIALOG
import ss.virovitica.sportsballtourney.dialog.RemoveSelfDialog
import ss.virovitica.sportsballtourney.dialog.RemoveSelfDialog.Companion.REMOVE_SELF_DIALOG
import ss.virovitica.sportsballtourney.dialog.SelectUsersDialog
import ss.virovitica.sportsballtourney.dialog.SelectUsersDialog.Companion.SELECT_USERS_DIALOG
import ss.virovitica.sportsballtourney.dialog.StartTournamentDialog
import ss.virovitica.sportsballtourney.dialog.StartTournamentDialog.Companion.START_TOURNAMENT_DIALOG
import ss.virovitica.sportsballtourney.fragment.tournamentinfo.presenter.ITournamentInfoPresenter
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.model.User

class TournamentInfoFragment: Fragment(), ITournamentInfoView, DeleteTournamentDialog.Listener, StartTournamentDialog.Listener,
    SelectUsersDialog.Listener, RemoveSelfDialog.Listener {

    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.fragment.tournamentinfo.view.TournamentInfoFragment.KEY_TOURNAMENT_ID"
    }

    //members
    override val ctx: Context
        get() = context!!
    private val presenter: ITournamentInfoPresenter by inject { parametersOf(this) }
    private var isGuest = true
    private var isOwner = false

    //functions

    //***Fragment***//

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_tournament_info, container, false)
        //set up buttons
        v.editUsersButton.setOnClickListener {
            val currentManagingUsers: List<User> = presenter.tournament.managers
            SelectUsersDialog(currentManagingUsers, this).show(activity!!.supportFragmentManager, SELECT_USERS_DIALOG)
        }
        v.editTeamsButton.setOnClickListener {
            ctx.startActivity<EditTeamsActivity>(EditTeamsActivity.KEY_TOURNAMENT_ID to presenter.tournament.id)
        }
        v.startTournamentButton.setOnClickListener {
            if(presenter.tournament.ready) {
                StartTournamentDialog(this).show(activity!!.supportFragmentManager, START_TOURNAMENT_DIALOG)
            }
            else ctx.toast(R.string.insufficient_teams)
        }
        v.deleteTournamentButton.setOnClickListener {
            DeleteTournamentDialog(this).show(activity!!.supportFragmentManager, DELETE_TOURNAMENT_DIALOG)
        }
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val tournamentID = arguments?.getString(KEY_TOURNAMENT_ID) ?: ""
        if(tournamentID != "") presenter.fetchTournament(tournamentID)
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    //***ITournamentInfoView***//

    override fun setGuestVisibility() {
        isGuest = true
        isOwner = false
        editUsersButton.visibility = GONE
        editTeamsButton.visibility = GONE
        startTournamentButton.visibility = GONE
        deleteTournamentButton.visibility = GONE
        deleteTournamentButton.requestLayout()
    }

    override fun setOwnerVisibility() {
        isGuest = false
        isOwner = true
        editUsersButton.visibility = VISIBLE
        editTeamsButton.visibility = VISIBLE
        startTournamentButton.visibility = VISIBLE
        deleteTournamentButton.visibility = VISIBLE
        deleteTournamentButton.requestLayout()
    }

    override fun setGeneralVisibility() {
        isGuest = false
        isOwner = false
        editUsersButton.visibility = GONE
        editTeamsButton.visibility = GONE
        startTournamentButton.visibility = GONE
        deleteTournamentButton.visibility = GONE
        deleteTournamentButton.requestLayout()
    }

    override fun fillViews(tournament: Tournament) {
        //hide the loading bar and show the list of managing users
        progressViewManagers?.visibility = GONE
        managingUsersLayout?.visibility = VISIBLE
        //fill info about managers
        managingUsersLayout?.removeAllViews()
        if(tournament.managers.size == 0) {
            val newView = layoutInflater.inflate(R.layout.view_manager, managingUsersLayout, true)
            newView.usernameText.text = getString(R.string.no_managers)
            //show the delete button so the manager-less tournament can be removed
            deleteTournamentButton?.visibility = VISIBLE
        }
        else {
            for(manager in tournament.managers) {
                val newView = layoutInflater.inflate(R.layout.view_manager, null, false)
                newView.usernameText.text = manager.name
                newView.userDetailText.text = manager.detail
                managingUsersLayout?.addView(newView)
            }
        }
        //fill info about teams
        teamsLayout?.removeAllViews()
        if(tournament.teamIDs.size == 0) {
            //hide the loading bar and show the (empty) list of teams
            progressViewTeams?.visibility = GONE
            teamsLayout?.visibility = VISIBLE
            //show the message about there being no teams
            teamsLayout?.removeAllViews()
            val newView = layoutInflater.inflate(R.layout.view_team, teamsLayout, true)
            newView.teamNameText.text = getString(R.string.no_teams)
        }
        else { presenter.fetchTeams(tournament.teamIDs) }
        if(isOwner) {
            if (tournament.teamIDs.size == Tournament.TOURNAMENT_SIZE) startTournamentButton?.visibility = VISIBLE
            else startTournamentButton?.visibility = GONE
        }
        if(tournament.started) {
            startTournamentButton?.visibility = GONE
        }
    }

    override fun addTeams(teams: List<Team>) {
        teamsLayout?.removeAllViews()
        //hide the loading bar and show the list of teams
        progressViewTeams?.visibility = GONE
        teamsLayout?.visibility = VISIBLE
        if(teams.isEmpty()) {
            //show the message about there being no teams
            val newView = layoutInflater.inflate(R.layout.view_team, teamsLayout, true)
            newView.teamNameText.text = getString(R.string.no_teams)
        }
        //put the data in
        for(team in teams) {
            val newView = layoutInflater.inflate(R.layout.view_team, null, false)
            newView.teamNameText.text = team.name
            newView.teamDetailText.text = team.playerIDs.size.toString()
            newView.setOnClickListener {
                activity!!.startActivity<EditPlayersActivity>(KEY_TEAM_ID to team.id, KEY_IS_OWNER to isOwner)
            }
            teamsLayout?.addView(newView)
        }
        teamsLayout?.requestLayout()
    }

    //***DeleteTournamentDialog.Listener***//

    override fun onConfirm() {
        if(activity is IMainView) {
            val parentView = activity as IMainView
            parentView.notifyOfDelete()
        }
        presenter.deleteTournament()
        startActivity(activity!!.intentFor<TournamentsActivity>().clearTop())
    }

    //***StartTournamentDialog.Listener***//

    override fun onConfirmStart() {
        presenter.startTournament()
    }

    //***SelectUsersDialog.Listener***//

    override fun onSaveManagers(managingUsers: List<User>, removedSelf: Boolean) {
        if(removedSelf) RemoveSelfDialog(this, managingUsers).show(activity!!.supportFragmentManager, REMOVE_SELF_DIALOG)
        else onConfirmChange(managingUsers)
    }

    //***RemoveSelfDialog.Listener***//

    override fun onConfirmChange(newManagingUsers: List<User>) {
        presenter.updateManagingUsers(newManagingUsers)
    }
}