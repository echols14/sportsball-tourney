package ss.virovitica.sportsballtourney.fragment.tournamentinfo.presenter

import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.model.User

interface ITournamentInfoPresenter {
    val tournament: Tournament
    fun fetchTournament(tournamentID: String)
    fun setVisibility()
    fun deleteTournament()
    fun fetchTeams(teamIDs: List<String>)
    fun updateManagingUsers(managingUsers: List<User>)
    fun unsubscribe()
    fun startTournament()
}