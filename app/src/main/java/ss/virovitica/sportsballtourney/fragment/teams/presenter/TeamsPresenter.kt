package ss.virovitica.sportsballtourney.fragment.teams.presenter

import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.fragment.teams.view.ITeamsView
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.countDifferencePoints
import ss.virovitica.sportsballtourney.utility.countMetaPoints
import ss.virovitica.sportsballtourney.utility.generateGroupStandings
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TeamsPresenter(private val view: ITeamsView, private val database: ILiveDatabase): ITeamsPresenter, ILiveDatabase.Listener {
    //members
    private lateinit var tournament: Tournament
    private var teams: List<Team> = ArrayList()
    private val matches: MutableMap<String, Match> = HashMap()
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()

    //functions

    //***ITeamsPresenter***//

    override fun fetchTournament(tournamentID: String) {
        database.getTournament(this, tournamentID)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.tournament = tournament
        database.getTeams(this, tournament.teamIDs)
        val allMatchIDs = LinkedList<String>()
        for(group in tournament.getGroups()) {
            allMatchIDs.addAll(group.getMatchIDs())
        }
        database.getMatches(this, allMatchIDs)
        view.setGroups(tournament.getGroups())
    }

    override fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.teams = teams
        view.setTeams(teams)
    }

    override fun matchesUpdate(matches: List<Match>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.matches.clear()
        for(match in matches) this.matches[match.id] = match
        //figure out standings
        val (groupStandings, _) = generateGroupStandings(tournament, this.matches)
        view.setStandings(groupStandings)
        //figure out meta-points and difference points
        val allMetaPointsMap = HashMap<String, Int>()
        val allDifferencePointsMap = HashMap<String, Int>()
        for(group in tournament.getGroups()) {
            val metaPoints = countMetaPoints(group, this.matches)
            allMetaPointsMap.putAll(metaPoints)
            val differencePoints = countDifferencePoints(group, this.matches)
            allDifferencePointsMap.putAll(differencePoints)
        }
        view.setMetaPoints(allMetaPointsMap)
        view.setDifferencePoints(allDifferencePointsMap)
    }

    override fun dataError(messageID: Int) {
        view.ctx.toast(messageID)
    }
}