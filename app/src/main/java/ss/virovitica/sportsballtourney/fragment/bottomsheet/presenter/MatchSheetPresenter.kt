package ss.virovitica.sportsballtourney.fragment.bottomsheet.presenter

import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.fragment.bottomsheet.view.IMatchSheetView
import ss.virovitica.sportsballtourney.model.Goal
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.model.Team
import java.util.*
import kotlin.collections.HashMap

class MatchSheetPresenter(private val view: IMatchSheetView, private val database: ILiveDatabase): IMatchSheetPresenter, ILiveDatabase.Listener {
    //members
    private lateinit var myMatch: Match
    override var teamA: Team? = null
        private set
    override var teamB: Team? = null
        private set
    override var playersMap: Map<String, Player> = HashMap()
        private set
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()

    //functions

    //***IMatchSheetPresenter***//

    override fun fetchMatch(matchID: String) {
        database.getMatch(this, matchID)
    }

    override fun addGoal(goal: Goal, byTeamA: Boolean) {
        //record the goal in the Player object
        val scoringPlayer = playersMap[goal.scoringPlayerID]
        if(scoringPlayer != null) {
            goal.scoringPlayerName = scoringPlayer.name
            scoringPlayer.goalValues.add(goal.value)
            database.updatePlayer(scoringPlayer)
        }
        //record the goal in the Match object
        if(byTeamA) myMatch.pointTeamA(goal)
        else myMatch.pointTeamB(goal)
        database.updateMatch(myMatch)
    }

    override fun finishMatch() {
        myMatch.finish()
        database.updateMatch(myMatch)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun matchUpdate(match: Match, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        myMatch = match
        view.setMatch(myMatch)
        database.getTeams(this, listOf(match.teamIdA, match.teamIdB))
    }

    override fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        for(team in teams) {
            if(team.id == myMatch.teamIdA) teamA = team
            if(team.id == myMatch.teamIdB) teamB = team
        }
        view.setTeams(teamA, teamB)
        val allPlayerIDs: MutableList<String> = LinkedList()
        val safeTeamA = teamA
        if(safeTeamA != null) allPlayerIDs.addAll(safeTeamA.playerIDs)
        val safeTeamB = teamB
        if(safeTeamB != null) allPlayerIDs.addAll(safeTeamB.playerIDs)
        database.getPlayers(this, allPlayerIDs)
    }

    override fun playersUpdate(players: List<Player>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        val playersMap: MutableMap<String, Player> = HashMap()
        for(player in players) {
            playersMap[player.id] = player
        }
        this.playersMap = playersMap
        view.setPlayers(playersMap)
    }

    override fun dataError(messageID: Int) {
        view.ctx.toast(messageID)
    }
}