package ss.virovitica.sportsballtourney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_team.view.*
import kotlinx.android.synthetic.main.view_team.view.teamDetailText
import org.jetbrains.anko.startActivity
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editplayers.view.EditPlayersActivity
import ss.virovitica.sportsballtourney.activity.editplayers.view.EditPlayersActivity.Companion.KEY_IS_OWNER
import ss.virovitica.sportsballtourney.activity.editplayers.view.EditPlayersActivity.Companion.KEY_TEAM_ID
import ss.virovitica.sportsballtourney.activity.editteams.view.IEditTeamsView
import ss.virovitica.sportsballtourney.model.Team

class EditTeamRecyclerAdapter(private val view: IEditTeamsView): RecyclerView.Adapter<EditTeamRecyclerAdapter.TeamHolder>() {
    //members
    private var teams: List<Team> = ArrayList()

    //functions

    fun setTeams(teams: List<Team>) {
        this.teams = teams
        notifyDataSetChanged()
    }

    //***RecyclerView.Adapter***//

    override fun getItemCount() = teams.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamHolder =
        TeamHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_team, parent, false))
    override fun onBindViewHolder(holder: TeamHolder, position: Int) = holder.bind(teams[position])

    //child class of ViewHolder
    inner class TeamHolder(parent: View) : RecyclerView.ViewHolder(parent), View.OnClickListener, View.OnLongClickListener {
        init {
            parent.setOnClickListener(this)
            parent.setOnLongClickListener(this)
        }

        //members
        private lateinit var context: Context
        private lateinit var team: Team

        //functions
        internal fun bind(team: Team) {
            this.team = team
            context = itemView.context
            //set fields
            itemView.teamNameText.text = team.name
            itemView.teamDetailText.text = team.playerIDs.size.toString()
        }
        override fun onClick(view: View) {
            context.startActivity<EditPlayersActivity>(KEY_TEAM_ID to team.id, KEY_IS_OWNER to true)
        }

        override fun onLongClick(v: View?): Boolean {
            view.editDialog(team)
            return true
        }
    }
}