package ss.virovitica.sportsballtourney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_player_detail.view.*
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.Player
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList

class PlayerDetailRecyclerAdapter: RecyclerView.Adapter<PlayerDetailRecyclerAdapter.PlayerHolder>() {
    //members
    private var players: List<Player> = ArrayList()

    //functions

    fun setPlayers(players: List<Player>) {
        this.players = players
        notifyDataSetChanged()
    }

    //***RecyclerView.Adapter***//

    override fun getItemCount() = players.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerHolder =
        PlayerHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_player_detail, parent, false))
    override fun onBindViewHolder(holder: PlayerHolder, position: Int) = holder.bind(players[position])

    //child class of ViewHolder
    inner class PlayerHolder(parent: View) : RecyclerView.ViewHolder(parent) {
        //members
        private lateinit var context: Context
        private lateinit var player: Player

        //functions
        internal fun bind(player: Player) {
            this.player = player
            context = itemView.context
            //set fields
            itemView.playerNameText.text = player.name
            if(player.number != null) {
                itemView.playerDetailText.text = context.getString(R.string.generic_number, player.number)
            }
            itemView.numberOfGoals.text = player.getTotalGoals().toString()
            itemView.numberOfPoints.text = player.getTotalPoints().toString()
            val df = DecimalFormat.getInstance(Locale.getDefault()) as DecimalFormat
            df.applyPattern("#.###")
            itemView.pointsPerGoal.text = df.format(player.getAveragePointsPerGoal())
        }
    }
}