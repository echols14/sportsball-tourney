package ss.virovitica.sportsballtourney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_tournament.view.*
import org.jetbrains.anko.startActivity
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.main.view.MainActivity
import ss.virovitica.sportsballtourney.activity.main.view.MainActivity.Companion.KEY_TOURNAMENT_ID
import ss.virovitica.sportsballtourney.model.Tournament

class TournamentRecyclerAdapter(private val currentUserID: String): RecyclerView.Adapter<TournamentRecyclerAdapter.TournamentHolder>() {
    //members
    private var tournaments: List<Tournament> = ArrayList()

    //functions

    fun setTournaments(tournaments: List<Tournament>) {
        this.tournaments = tournaments.sortedWith(compareBy { it.name })
        notifyDataSetChanged()
    }

    override fun getItemCount() = tournaments.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TournamentHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_tournament, parent, false))
    override fun onBindViewHolder(holder: TournamentHolder, position: Int) { holder.bind(tournaments[position]) }

    //child class of ViewHolder
    inner class TournamentHolder(parent: View) : RecyclerView.ViewHolder(parent), View.OnClickListener {
        init { parent.setOnClickListener(this) }

        //members
        private lateinit var context: Context
        private lateinit var tournament: Tournament

        //functions
        internal fun bind(tournament: Tournament) {
            this.tournament = tournament
            context = itemView.context
            //set fields
            var isOwner = false
            for(user in tournament.managers) {
                if(user.id == currentUserID) {
                    isOwner = true
                    break
                }
            }
            if(isOwner) itemView.tournamentNameText.text = context.getString(R.string.generic_starred, tournament.name)
            else itemView.tournamentNameText.text = tournament.name
            itemView.inProgressText.text =
                if(!tournament.started) context.getString(R.string.not_started)
                else {
                    if(!tournament.finished) context.getString(R.string.in_progress)
                    else context.getString(R.string.finished)
                }
        }
        override fun onClick(view: View) {
            context.startActivity<MainActivity>(KEY_TOURNAMENT_ID to tournament.id)
        }
    }
}