package ss.virovitica.sportsballtourney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_user_select.view.*
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.User
import java.util.*
import kotlin.collections.ArrayList

class ManagingUsersRecyclerAdapter(private val initialManagingUsers: List<User>): RecyclerView.Adapter<ManagingUsersRecyclerAdapter.UserHolder>() {
    //members
    private var users: List<User> = ArrayList()
    private var checks: MutableList<Boolean> = ArrayList()

    //functions

    fun setUsers(users: List<User>) {
        this.users = users
        checks = ArrayList(users.size)
        for(user in users) {
            checks.add(initialManagingUsers.contains(user))
        }
        notifyDataSetChanged()
    }

    fun getSelectedUsers(): List<User> {
        val checkedUsers: MutableList<User> = LinkedList()
        for((i, check) in checks.withIndex()) {
            if(check) checkedUsers.add(users[i])
        }
        return checkedUsers
    }

    override fun getItemCount() = users.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UserHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_user_select, parent, false))
    override fun onBindViewHolder(holder: UserHolder, position: Int) { holder.bind(users[position], position) }

    //child class of ViewHolder
    inner class UserHolder(parent: View) : RecyclerView.ViewHolder(parent) {
        //members
        private lateinit var context: Context
        private lateinit var user: User
        private var index: Int = -1

        //functions
        internal fun bind(user: User, position: Int) {
            this.user = user
            index = position
            context = itemView.context
            //set fields
            itemView.usernameText.text = user.name
            itemView.userDetailText.text = user.detail
            itemView.checkbox.isChecked = checks[index]
            itemView.checkbox.setOnCheckedChangeListener { _, isChecked ->
                checks[index] = isChecked
            }
        }
    }
}