package ss.virovitica.sportsballtourney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_player.view.*
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editplayers.view.IEditPlayersView
import ss.virovitica.sportsballtourney.model.Player

class EditPlayerRecyclerAdapter(private val parentView: IEditPlayersView, private val isOwner: Boolean): RecyclerView.Adapter<EditPlayerRecyclerAdapter.PlayerHolder>() {
    //members
    private var players: List<Player> = ArrayList()

    //functions

    fun setPlayers(players: List<Player>) {
        this.players = players
        notifyDataSetChanged()
    }

    //***RecyclerView.Adapter***//

    override fun getItemCount() = players.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerHolder =
        PlayerHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_player, parent, false))
    override fun onBindViewHolder(holder: PlayerHolder, position: Int) = holder.bind(players[position])

    //child class of ViewHolder
    inner class PlayerHolder(parent: View) : RecyclerView.ViewHolder(parent), View.OnClickListener {
        init { parent.setOnClickListener(this) }

        //members
        private lateinit var context: Context
        private lateinit var player: Player

        //functions
        internal fun bind(player: Player) {
            this.player = player
            context = itemView.context
            //set fields
            itemView.playerNameText.text = player.name
            if(player.number != null) {
                itemView.playerDetailText.text = context.getString(R.string.generic_number, player.number)
            }
        }
        override fun onClick(view: View) {
            if(isOwner) parentView.editDialog(player)
        }
    }
}