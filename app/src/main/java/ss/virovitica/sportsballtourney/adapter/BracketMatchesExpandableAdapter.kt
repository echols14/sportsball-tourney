package ss.virovitica.sportsballtourney.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import kotlinx.android.synthetic.main.view_bracket_level.view.*
import kotlinx.android.synthetic.main.view_match_bracket.view.*
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.FINAL_INDEX
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.FINAL_SIZE
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.LEVELS
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.QUARTER_FINAL_INDEX
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.QUARTER_FINAL_SIZE
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.SEMI_FINAL_INDEX
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.SEMI_FINAL_SIZE
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.BREAK_INDEX_1
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.BREAK_INDEX_2
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter.Companion.BREAK_INDEX_3
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team

class BracketMatchesExpandableAdapter: BaseExpandableListAdapter() {
    //members
    private var teams: Map<String, Team> = HashMap()
    private var quarterFinalMatches: List<Match> = ArrayList(QUARTER_FINAL_SIZE)
    private var semiFinalMatches: List<Match> = ArrayList(SEMI_FINAL_SIZE)
    private var finalMatch: List<Match> = ArrayList(FINAL_SIZE)

    //functions

    fun setTeams(teamsIn: List<Team>) {
        val newTeamsMap = HashMap<String, Team>()
        for(team in teamsIn) {
            newTeamsMap[team.id] = team
        }
        teams = newTeamsMap
        notifyDataSetChanged()
    }
    fun setMatches(matchesIn: List<Match>) {
        if(matchesIn.size >= BREAK_INDEX_1) quarterFinalMatches = matchesIn.subList(0, BREAK_INDEX_1)
        if(matchesIn.size >= BREAK_INDEX_2) semiFinalMatches = matchesIn.subList(BREAK_INDEX_1, BREAK_INDEX_2)
        if(matchesIn.size >= BREAK_INDEX_3) finalMatch = matchesIn.subList(BREAK_INDEX_2, BREAK_INDEX_3)
        notifyDataSetChanged()
    }
    fun isLevelFinished(groupPosition: Int): Boolean {
        //figure out if this level is finished
        val thisLevel = getGroup(groupPosition)
        if(thisLevel.isEmpty()) return false
        for(match in thisLevel) {
            if(!match.finished) {
                return false
            }
        }
        return true
    }

    override fun getGroup(groupPosition: Int): List<Match> =
        when(groupPosition) {
            QUARTER_FINAL_INDEX -> quarterFinalMatches
            SEMI_FINAL_INDEX -> semiFinalMatches
            FINAL_INDEX -> finalMatch
            else -> ArrayList()
        }
    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = true
    override fun hasStableIds() = false
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val groupView = inflater.inflate(R.layout.view_bracket_level, parent, false)
        //figure out if this level is finished
        val levelFinished = isLevelFinished(groupPosition)
        //set background color to indicate if the level is finished or not
        val colorID =
            if(levelFinished) R.color.colorLightGreen
            else R.color.colorLightRed
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) groupView.setBackgroundColor(parent.resources.getColor(colorID, null))
        else groupView.setBackgroundColor(parent.resources.getColor(R.color.colorLightGreen))
        //set remaining views
        val textID = when(groupPosition) {
            QUARTER_FINAL_INDEX -> R.string.quarter_finals
            SEMI_FINAL_INDEX -> R.string.semi_finals
            FINAL_INDEX -> R.string.final_word
            else -> -1
        }
        if(textID != -1) groupView.bracketLevelText.text = parent.context.getString(textID)
        return groupView
    }
    override fun getChildrenCount(groupPosition: Int) =
        when(groupPosition) {
            QUARTER_FINAL_INDEX -> quarterFinalMatches.size
            SEMI_FINAL_INDEX -> semiFinalMatches.size
            FINAL_INDEX -> finalMatch.size
            else -> 0
        }
    override fun getChild(groupPosition: Int, childPosition: Int): Match =
        when(groupPosition) {
            QUARTER_FINAL_INDEX -> quarterFinalMatches[childPosition]
            SEMI_FINAL_INDEX -> semiFinalMatches[childPosition]
            FINAL_INDEX -> finalMatch[childPosition]
            else -> Match()
        }
    override fun getGroupId(groupPosition: Int) = groupPosition.toLong()
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val childView = inflater.inflate(R.layout.view_match_bracket, parent, false)
        val match = getChild(groupPosition, childPosition)
        //set background color to indicate if the match is finished or not
        val colorID =
            if(match.finished) R.color.colorLightGreen
            else R.color.colorLightRed
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) childView.setBackgroundColor(parent.resources.getColor(colorID, null))
        else childView.setBackgroundColor(parent.resources.getColor(colorID))
        //set remaining views
        val teamA = teams[match.teamIdA]
        val teamB = teams[match.teamIdB]
        childView.nameTeamA.text = teamA?.name
        childView.nameTeamB.text = teamB?.name
        childView.scoreTeamA.text = match.scoreTeamA.toString()
        childView.scoreTeamB.text = match.scoreTeamB.toString()
        if(match.finished) {
            if (match.scoreTeamA > match.scoreTeamB) childView.victorName.text = teamA?.name
            else childView.victorName.text = teamB?.name
        }
        return childView
    }
    override fun getChildId(groupPosition: Int, childPosition: Int) = (groupPosition*childPosition).toLong()
    override fun getGroupCount() = LEVELS
}