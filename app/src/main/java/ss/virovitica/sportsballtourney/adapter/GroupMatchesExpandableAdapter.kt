package ss.virovitica.sportsballtourney.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import kotlinx.android.synthetic.main.view_group_preview.view.*
import kotlinx.android.synthetic.main.view_match_preview.view.*
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.Group
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Team

class GroupMatchesExpandableAdapter: BaseExpandableListAdapter() {
    //members
    private var groups: List<Group> = ArrayList()
    private var teams: Map<String, Team> = HashMap()
    private var matches: Map<String, Match> = HashMap()

    //functions

    fun setGroups(groupsIn: List<Group>) {
        groups = groupsIn
        notifyDataSetChanged()
    }
    fun setTeams(teamsIn: List<Team>) {
        val newTeamsMap = HashMap<String, Team>()
        for(team in teamsIn) {
            newTeamsMap[team.id] = team
        }
        teams = newTeamsMap
        notifyDataSetChanged()
    }
    fun setMatches(matchesIn: List<Match>) {
        val newMatchesMap = HashMap<String, Match>()
        for(match in matchesIn) {
            newMatchesMap[match.id] = match
        }
        matches = newMatchesMap
        notifyDataSetChanged()
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = true
    override fun hasStableIds() = false
    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()
    override fun getChildId(groupPosition: Int, childPosition: Int): Long = (groupPosition*childPosition).toLong()
    override fun getGroupCount() = groups.size
    override fun getChildrenCount(groupPosition: Int) = groups[groupPosition].getMatchIDs().size
    override fun getGroup(groupPosition: Int) = groups[groupPosition]
    override fun getChild(groupPosition: Int, childPosition: Int) = matches[groups[groupPosition].getMatchIDs()[childPosition]] ?: Match()
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val groupView = inflater.inflate(R.layout.view_group_preview, parent, false)
        //set background color to indicate if the group of matches is finished or not
        val colorID =
            if(getGroup(groupPosition).finished) R.color.colorLightGreen
            else R.color.colorLightRed
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) groupView.setBackgroundColor(parent.resources.getColor(colorID, null))
        else groupView.setBackgroundColor(parent.resources.getColor(R.color.colorLightGreen))
        //set remaining views
        val group = getGroup(groupPosition)
        groupView.matchTitleText.text = parent.context.getString(R.string.generic_group_name, group.groupNumber)
        return groupView
    }
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val childView = inflater.inflate(R.layout.view_match_preview, parent, false)
        val match = getChild(groupPosition, childPosition)
        //set background color to indicate if the match is finished or not
        val colorID =
            if(match.finished) R.color.colorLightGreen
            else R.color.colorLightRed
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) childView.setBackgroundColor(parent.resources.getColor(colorID, null))
        else childView.setBackgroundColor(parent.resources.getColor(colorID))
        //set remaining views
        childView.scoreTeamA.text = match.scoreTeamA.toString()
        childView.scoreTeamB.text = match.scoreTeamB.toString()
        childView.nameTeamA.text = teams[match.teamIdA]?.name
        childView.nameTeamB.text = teams[match.teamIdB]?.name
        return childView
    }
}