package ss.virovitica.sportsballtourney.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import kotlinx.android.synthetic.main.view_group_preview.view.*
import kotlinx.android.synthetic.main.view_team_standing.view.*
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.Group
import ss.virovitica.sportsballtourney.model.Team

class TeamExpandableAdapter: BaseExpandableListAdapter() {
    //members
    private var groups: List<Group> = ArrayList()
    private var teams: Map<String, Team> = HashMap()
    private var groupStandings: Map<Int, List<String>> = HashMap()
    private var metaPoints: Map<String, Int> = HashMap()
    private var differencePoints: Map<String, Int> = HashMap()

    //functions

    fun setGroups(groupsIn: List<Group>) {
        groups = groupsIn
        notifyDataSetChanged()
    }
    fun setTeams(teamsIn: List<Team>) {
        val newTeamsMap = HashMap<String, Team>()
        for(team in teamsIn) {
            newTeamsMap[team.id] = team
        }
        teams = newTeamsMap
        notifyDataSetChanged()
    }
    fun setStandings(groupStandings: Map<Int, List<String>>) {
        this.groupStandings = groupStandings
        notifyDataSetChanged()
    }
    fun setMetaPoints(metaPointsIn: Map<String, Int>) {
        metaPoints = metaPointsIn
        notifyDataSetChanged()
    }
    fun setDifferencePoints(differencePointsIn: Map<String, Int>) {
        differencePoints = differencePointsIn
        notifyDataSetChanged()
    }

    override fun getGroup(groupPosition: Int) = groups[groupPosition]
    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = true
    override fun hasStableIds() = false
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val groupView = inflater.inflate(R.layout.view_group_preview, parent, false)
        val group = getGroup(groupPosition)
        //set background color to indicate if the group of matches is finished or not
        val colorID =
            if(group.finished) R.color.colorLightGreen
            else R.color.colorLightRed
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) groupView.setBackgroundColor(parent.resources.getColor(colorID, null))
        else groupView.setBackgroundColor(parent.resources.getColor(R.color.colorLightGreen))
        //set remaining views
        groupView.matchTitleText.text = parent.context.getString(R.string.generic_group_name, group.groupNumber)
        return groupView
    }
    override fun getChildrenCount(groupPosition: Int) = getGroup(groupPosition).getTeamIDs().size
    override fun getChild(groupPosition: Int, childPosition: Int): Team {
        val group = getGroup(groupPosition)
        val standingsList = groupStandings[group.groupNumber]
        return if(standingsList == null) Team()
                else {
                    if(childPosition >= standingsList.size) Team()
                    else teams[standingsList[childPosition]] ?: Team()
                }
    }
    override fun getGroupId(groupPosition: Int) = groupPosition.toLong()
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val childView = inflater.inflate(R.layout.view_team_standing, parent, false)
        val team = getChild(groupPosition, childPosition)
        //team info
        childView.teamRankingText.text = parent.context.getString(R.string.generic_number, childPosition + 1)
        childView.teamNameText.text = team.name
        //meta-points
        val metaPointVal = metaPoints[team.id]
        if(metaPointVal != null) childView.metaPointsText.text = metaPointVal.toString()
        //difference in points for this team and points against this team
        val differencePointVal = differencePoints[team.id]
        if(differencePointVal != null) childView.pointsDifferenceText.text =
            parent.context.getString(R.string.number_in_parentheses, differencePointVal)
        return childView
    }
    override fun getChildId(groupPosition: Int, childPosition: Int): Long = (groupPosition*childPosition).toLong()
    override fun getGroupCount() = groups.size
}