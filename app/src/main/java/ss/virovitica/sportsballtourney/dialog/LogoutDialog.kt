package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ss.virovitica.sportsballtourney.R

class LogoutDialog(private val listener: Listener): DialogFragment() {
    companion object {
        const val LOGOUT_DIALOG = "ss.virovitica.sportsballtourney.dialog.LogoutDialog"
    }

    interface Listener {
        fun onLogout()
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            //get the layout inflater
            builder.setTitle(R.string.logout)
                .setMessage(R.string.logout_confirm_message)
                .setPositiveButton(R.string.logout) { _, _ ->
                    listener.onLogout()
                    dialog.dismiss()
                }
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}