package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_new_team.*
import kotlinx.android.synthetic.main.dialog_new_team.view.*
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.Team

class TeamDialog(private val listener: Listener, private val team: Team?, private val deletable: Boolean): DialogFragment() {
    companion object {
        const val TEAM_DIALOG = "ss.virovitica.sportsballtourney.dialog.TeamDialog"
    }

    interface Listener {
        fun onSubmit(team: Team, isEdit: Boolean)
        fun onDelete(teamID: String)
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_new_team, null)
            //pieces common to both edit player and new player
            builder.setView(dialogView)
                .setPositiveButton(R.string.ok) { _, _ -> } //see onStart for positive action
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            //edit team
            if(team != null) {
                builder.setTitle(R.string.edit_team)
                if(deletable) builder.setNegativeButton(R.string.delete) { _, _ -> listener.onDelete(team.id) }
                dialogView.teamNameInput.setText(team.name)
            }
            //new team
            else builder.setTitle(R.string.new_team)
            //build
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).setOnClickListener {
            //setting the button action like this removes the automatic call to dismiss()
            val teamName = dialog.teamNameInput.editableText.toString()
            val thisContext = context
            if(teamName == "") thisContext?.toast(thisContext.getString(R.string.empty_field))
            else {
                if(team != null) {
                    team.name = teamName
                    listener.onSubmit(team, true)
                }
                else listener.onSubmit(Team(teamName), false)
                dialog.dismiss()
            }
        }
    }
}