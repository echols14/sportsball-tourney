package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_player.*
import kotlinx.android.synthetic.main.dialog_player.view.*
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.Player

class PlayerDialog(private val listener: Listener, private val player: Player?): DialogFragment() {
    companion object {
        const val PLAYER_DIALOG = "ss.virovitica.sportsballtourney.dialog.PlayerDialog"
    }

    interface Listener {
        fun onSubmit(player: Player, isEdit: Boolean)
        fun onDelete(playerID: String)
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_player, null)
            //pieces common to both edit player and new player
            builder.setView(dialogView)
                .setPositiveButton(R.string.ok) { _, _ -> } //see onStart for positive action
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            //edit player
            if(player != null) {
                builder.setTitle(R.string.edit_player)
                    .setNegativeButton(R.string.delete) { _, _ -> listener.onDelete(player.id) }
                dialogView.playerNameInput.setText(player.name)
                dialogView.playerNumberInput.setText(player.numberText)
            }
            //new player
            else builder.setTitle(R.string.new_player)
            //build
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).setOnClickListener {
            //setting the button action like this removes the automatic call to dismiss()
            val playerName = dialog.playerNameInput.editableText.toString()
            val playerNumberString = dialog.playerNumberInput.editableText.toString()
            var playerNumber: Int?
            try {
                playerNumber = playerNumberString.toInt()
            }
            catch(e: NumberFormatException) {
                playerNumber = null
            }
            val thisContext = context
            if(playerName == "") thisContext?.toast(thisContext.getString(R.string.empty_field))
            else {
                if(player != null) {
                    player.name = playerName
                    player.number = playerNumber
                    listener.onSubmit(player, true)
                }
                else listener.onSubmit(Player(playerName, playerNumber), false)
                dialog.dismiss()
            }
        }
    }
}