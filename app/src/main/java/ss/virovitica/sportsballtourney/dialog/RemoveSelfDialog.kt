package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.User

class RemoveSelfDialog(private val listener: Listener, private val newManagingUsers: List<User>): DialogFragment() {
    companion object {
        const val REMOVE_SELF_DIALOG = "ss.virovitica.sportsballtourney.dialog.RemoveSelfDialog"
    }

    interface Listener {
        fun onConfirmChange(newManagingUsers: List<User>)
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            builder.setTitle(R.string.relinquish_privileges)
                .setMessage(R.string.relinquish_privileges_confirmation)
                .setPositiveButton(R.string.remove_me) { _, _ ->
                    listener.onConfirmChange(newManagingUsers)
                    dialog.dismiss()
                }
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}