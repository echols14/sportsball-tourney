package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.dialog_select_users.view.*
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.adapter.ManagingUsersRecyclerAdapter
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.User
import java.util.*

class SelectUsersDialog(initialManagingUsers: List<User>, private val listener: Listener): DialogFragment(), ILiveDatabase.Listener {
    companion object {
        const val SELECT_USERS_DIALOG = "ss.virovitica.sportsballtourney.dialog.SelectUsersDialog"
    }

    interface Listener {
        fun onSaveManagers(managingUsers: List<User>, removedSelf: Boolean)
    }

    //members
    private lateinit var dialogView: View
    private val auth: FirebaseAuth by inject()
    private val database: ILiveDatabase by inject()
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private val adapter = ManagingUsersRecyclerAdapter(initialManagingUsers)
    private lateinit var users: List<User>
    private var loaded = false

    //functions

    fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***DialogFragment***//

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            dialogView = inflater.inflate(R.layout.dialog_select_users, null)
            //set up the recycler
            dialogView.usersRecycler.layoutManager = LinearLayoutManager(activity)
            dialogView.usersRecycler.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
            dialogView.usersRecycler.adapter = adapter
            //get the list of users
            database.getAllUsers(this)
            //build the dialog
            builder.setView(dialogView)
                .setTitle(R.string.select_managing_users)
                .setPositiveButton(R.string.save) { _, _ ->
                    if(!loaded) dialog.cancel()
                    else {
                        //find out which users are currently checked
                        val selectedUsers = adapter.getSelectedUsers()
                        //check if the current user is still in the list of managing users
                        val currentUser = auth.currentUser
                        if (currentUser == null) {
                            context?.toast(R.string.unauthorized_action)
                            Log.e(
                                "SelectUsersDialog",
                                "Anonymous user pressed the save button to change managing users"
                            )
                        } else {
                            var containsCurrentUser = false
                            for (user in selectedUsers) {
                                if (user.id == currentUser.uid) {
                                    containsCurrentUser = true
                                    break
                                }
                            }
                            //send the result to be handled
                            listener.onSaveManagers(selectedUsers, !containsCurrentUser)
                        }
                        dialog.dismiss()
                    }
                }
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onDismiss(dialog: DialogInterface?) {
        unsubscribe()
        super.onDismiss(dialog)
    }

    //***ILiveDatabase.Listener***//

    override fun usersAllUpdate(users: List<User>, listener: ValueEventListener) {
        loaded = true
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.users = users.sortedWith(compareBy { it.name })
        //hide the loading bar and show the list
        dialogView.progressView.visibility = GONE
        if(users.isEmpty()) {
            dialogView.usersRecycler.visibility = GONE
                dialogView.emptyView.visibility = VISIBLE
        }
        else dialogView.usersRecycler.visibility = VISIBLE
        //put the data in
        adapter.setUsers(this.users)
    }

    override fun dataError(messageID: Int) {
        context?.toast(messageID)
        Log.i("SelectUsersDialog", "dataError() called")
    }
}