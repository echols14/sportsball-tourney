package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ss.virovitica.sportsballtourney.R

class FinalizeResultsDialog(private val listener: Listener): DialogFragment() {
    companion object {
        const val FINALIZE_RESULTS_DIALOG = "ss.virovitica.sportsballtourney.dialog.FinalizeResultsDialog"
    }

    interface Listener {
        fun onFinalize()
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.finalize_results)
                .setMessage(R.string.finalize_results_confirmation)
                .setPositiveButton(R.string.finalize) { _, _ ->
                    listener.onFinalize()
                    dialog.dismiss()
                }
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}