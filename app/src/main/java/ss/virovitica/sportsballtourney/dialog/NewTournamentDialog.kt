package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_new_tournament.*
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R

class NewTournamentDialog(private val listener: Listener): DialogFragment() {
    companion object {
        const val NEW_TOURNAMENT_DIALOG = "ss.virovitica.sportsballtourney.dialog.NewTournamentDialog"
    }

    interface Listener {
        fun onSubmit(tourneyName: String)
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_new_tournament, null)
            builder.setView(dialogView)
                .setTitle(R.string.new_tournament)
                .setPositiveButton(R.string.ok) { _, _ -> } //see onStart for positive action
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).setOnClickListener {
            //setting the button action like this removes the automatic call to dismiss()
            val tourneyName = dialog.tournamentNameInput.editableText.toString()
            val thisContext = context
            if(tourneyName == "") thisContext?.toast(thisContext.getString(R.string.empty_field))
            else {
                listener.onSubmit(tourneyName)
                dialog.dismiss()
            }
        }
    }
}