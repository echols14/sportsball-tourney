package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import androidx.fragment.app.DialogFragment
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.Goal
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.dialog_add_goal.view.*
import kotlinx.android.synthetic.main.dialog_add_goal.view.scoringTeamSpinner
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.model.Team

class AddGoalDialog(private val listener: Listener, private val teamA: Team, private val teamB: Team,
                    private val playersMap: Map<String, Player>): DialogFragment() {

    companion object {
        const val ADD_GOAL_DIALOG = "ss.virovitica.sportsballtourney.dialog.AddGoalDialog"
    }

    //members
    private lateinit var dialogView: View
    private var isTeamA: Boolean = true
    private var scoringPlayerID = ""
    private var scoreMinute = -1
    private var goalValue = 0

    interface Listener {
        fun onSaveGoal(goal: Goal, byTeamA: Boolean)
    }

    //functions

    //***DialogFragment***//

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            //get the layout inflater
            val inflater = requireActivity().layoutInflater
            dialogView = inflater.inflate(R.layout.dialog_add_goal, null)
            //initialize spinner to pick a team
            val teamArray = arrayOf(teamA.name, teamB.name)
            val teamAdapter = ArrayAdapter(it, android.R.layout.simple_spinner_item, teamArray)
            teamAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            dialogView.scoringTeamSpinner.adapter = teamAdapter
            dialogView.scoringTeamSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    //initialize spinner to pick which player
                    isTeamA = (position == 0)
                    setPlayersSpinner(it, isTeamA)
                }
            }
            //set up the editText for the time of the goal
            dialogView.goalTimeInput.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) { scoreMinute = s.toString().toIntOrNull() ?: -1 }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
            //set up the editText for the value of the goal
            dialogView.goalValueInput.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) { goalValue = s.toString().toIntOrNull() ?: 0 }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
            //build the dialog
            builder.setView(dialogView)
                .setTitle(R.string.add_results)
                .setPositiveButton(R.string.save) { _, _ -> }  //see onStart for positive action
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(Dialog.BUTTON_POSITIVE).setOnClickListener {
            //setting the button action like this removes the automatic call to dismiss()
            val thisContext = context
            if(dialogView.goalTimeInput.text.isNullOrEmpty() || dialogView.goalValueInput.text.isNullOrEmpty()) {
                thisContext?.toast(thisContext.getString(R.string.empty_field))
            }
            else if(scoreMinute < 0) thisContext?.toast(thisContext.getString(R.string.invalid_time))
            else {
                listener.onSaveGoal(Goal(scoreMinute, scoringPlayerID, goalValue), isTeamA)
                dialog.dismiss()
            }
        }
    }

    //private

    private fun setPlayersSpinner(context: Context, isTeamA: Boolean) {
        //get values to put in the list
        val targetPlayerList = if(isTeamA) teamA.playerIDs else teamB.playerIDs
        val playerNames: MutableList<String> = ArrayList(targetPlayerList.size)
        for(playerID in targetPlayerList) {
            val player = playersMap[playerID]
            if(player == null) { //couldn't find the player
                playerNames.add("")
                continue
            }
            val playerNumber = player.number
            val nameValue = //does the player have a number?
                if(playerNumber != null) context.getString(R.string.generic_player, playerNumber, player.name)
                else player.name
            playerNames.add(nameValue)
        }
        //fill the spinner with the values
        val playerNamesArray = playerNames.toTypedArray()
        val playersAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, playerNamesArray)
        playersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dialogView.scoringPlayerSpinner.adapter = playersAdapter
        dialogView.scoringPlayerSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) { scoringPlayerID = "" }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                scoringPlayerID = if(isTeamA) teamA.playerIDs[position] else teamB.playerIDs[position]
            }
        }
        //if there are no players on the selected team, hide the player selection (for aesthetics)
        if(playerNamesArray.isEmpty()) {
            dialogView.scoringPlayerLabel.visibility = GONE
            dialogView.scoringPlayerSpinner.visibility = GONE
        }
        else {
            dialogView.scoringPlayerLabel.visibility = VISIBLE
            dialogView.scoringPlayerSpinner.visibility = VISIBLE
        }
    }
}