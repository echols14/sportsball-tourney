package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ss.virovitica.sportsballtourney.R

class StartTournamentDialog(private val listener: Listener): DialogFragment() {
    companion object {
        const val START_TOURNAMENT_DIALOG = "ss.virovitica.sportsballtourney.dialog.StartTournamentDialog"
    }

    interface Listener {
        fun onConfirmStart()
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            builder.setTitle(R.string.start_tournament)
                .setMessage(R.string.start_tournament_confirm_message)
                .setPositiveButton(R.string.start) { _, _ ->
                    listener.onConfirmStart()
                    dialog.dismiss()
                }
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}