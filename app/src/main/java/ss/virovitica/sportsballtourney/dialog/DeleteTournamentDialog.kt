package ss.virovitica.sportsballtourney.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ss.virovitica.sportsballtourney.R

class DeleteTournamentDialog(private val listener: Listener): DialogFragment() {
    companion object {
        const val DELETE_TOURNAMENT_DIALOG = "ss.virovitica.sportsballtourney.dialog.DeleteTournamentDialog"
    }

    interface Listener {
        fun onConfirm()
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.delete_tournament)
                .setMessage(R.string.delete_tournament_confirm_message)
                .setPositiveButton(R.string.delete) { _, _ ->
                    listener.onConfirm()
                    dialog.dismiss()
                }
                .setNeutralButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}