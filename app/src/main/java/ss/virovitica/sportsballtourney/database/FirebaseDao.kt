package ss.virovitica.sportsballtourney.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.model.*
import java.util.*

class FirebaseDao: ILiveDatabase {
    companion object {
        private val database = FirebaseDatabase.getInstance()
        const val USER_TABLE = "users"
        const val TOURNAMENT_TABLE = "tournaments"
        const val TEAM_TABLE = "teams"
        const val PLAYER_TABLE = "players"
        const val MATCH_TABLE = "matches"
    }

    //functions

    private fun logError(e: DatabaseError) = Log.e("FirebaseDao", "Database read failed: " + e.message)
    
    //***ILiveDatabase***//

    override fun removeListener(listener: ValueEventListener) {
        database.getReference(USER_TABLE).removeEventListener(listener)
        database.getReference(TOURNAMENT_TABLE).removeEventListener(listener)
        database.getReference(TEAM_TABLE).removeEventListener(listener)
        database.getReference(PLAYER_TABLE).removeEventListener(listener)
        database.getReference(MATCH_TABLE).removeEventListener(listener)
    }
    
    //users
    
    override fun saveUser(user: User) {
        val table = database.getReference(USER_TABLE)
        table.child(user.id).setValue(user)
    }

    override fun getAllUsers(listener: ILiveDatabase.Listener) {
        val table = database.getReference(USER_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) { logError(e) }
            override fun onDataChange(data: DataSnapshot) {
                val newUsers = LinkedList<User>()
                data.children.forEach {
                    val user = it.getValue(User::class.java)
                    if(user != null) newUsers.add(user)
                }
                listener.usersAllUpdate(newUsers, this)
            }
        })
    }

    override fun getUser(userID: String, listener: ILiveDatabase.Listener) {
        val table = database.getReference(USER_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                var error = false
                if(!data.hasChild(userID)) error = true
                else {
                    val user = data.child(userID).getValue(User::class.java)
                    if (user != null && user.id == userID) {
                        listener.userUpdate(user, this)
                    } else error = true
                }
                if(error) {
                    //listener.dataError(R.string.user_not_found)
                    Log.i("FirebaseDao.getUser", "user requested but not found")
                }
            }
        })
    }
    
    //tournaments

    override fun addTournament(tournament: Tournament) {
        val table = database.getReference(TOURNAMENT_TABLE)
        table.child(tournament.id).setValue(tournament)
    }

    override fun getAllTournaments(listener: ILiveDatabase.Listener) {
        val table = database.getReference(TOURNAMENT_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) { logError(e) }
            override fun onDataChange(data: DataSnapshot) {
                val newTournaments = LinkedList<Tournament>()
                data.children.forEach {
                    val tournament = it.getValue(Tournament::class.java)
                    if(tournament != null) newTournaments.add(tournament)
                }
                listener.tournamentsAllUpdate(newTournaments, this)
            }
        })
    }

    override fun getTournament(listener: ILiveDatabase.Listener, tournamentID: String) {
        val table = database.getReference(TOURNAMENT_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                var error = false
                if(!data.hasChild(tournamentID)) error = true
                else {
                    val tournament = data.child(tournamentID).getValue(Tournament::class.java)
                    if (tournament != null && tournament.id == tournamentID) {
                        listener.tournamentUpdate(tournament, this)
                    } else error = true
                }
                if(error) {
                    //listener.dataError(R.string.tournament_not_found)
                    Log.i("FirebaseDao.getTourname", "tournament requested but not found")
                }
            }
        })
    }

    override fun deleteTournament(tournamentID: String) {
        val table = database.getReference(TOURNAMENT_TABLE)
        table.child(tournamentID).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(data: DataSnapshot) {
                val tournament = data.getValue(Tournament::class.java)
                if(tournament != null && tournament.id == tournamentID) {
                    for(teamID in tournament.teamIDs) {
                        deleteTeam(teamID)
                    }
                    for(matchID in tournament.getBracketMatchIDs()) {
                        deleteMatch(matchID)
                    }
                    for(group in tournament.getGroups()) {
                        for(matchID in group.getMatchIDs()) {
                            deleteMatch(matchID)
                        }
                    }
                }
            }
            override fun onCancelled(e: DatabaseError) { logError(e) }
        })
        table.child(tournamentID).removeValue()
    }

    override fun updateTournament(tournament: Tournament) {
        val table = database.getReference(TOURNAMENT_TABLE)
        table.child(tournament.id).setValue(tournament)
    }
    
    //teams

    override fun addTeam(team: Team) {
        val table = database.getReference(TEAM_TABLE)
        table.child(team.id).setValue(team)
    }

    override fun getTeams(listener: ILiveDatabase.Listener, teamIDs: List<String>) {
        val table = database.getReference(TEAM_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                val teams = LinkedList<Team>()
                var teamMissing = false
                for(teamID in teamIDs) {
                    var error = false
                    if(!data.hasChild(teamID)) error = true
                    else {
                        val team = data.child(teamID).getValue(Team::class.java)
                        if (team != null && team.id == teamID) {
                            teams.add(team)
                        } else error = true
                    }
                    if(error) teamMissing = true
                }
                if(teamMissing) {
                    //listener.dataError(R.string.team_not_found)
                    Log.i("FirebaseDao.getTeams", "team requested but not found")
                }
                listener.teamsUpdate(teams, this)
            }
        })
    }

    override fun getTeam(listener: ILiveDatabase.Listener, teamID: String) {
        val table = database.getReference(TEAM_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                var error = false
                if(!data.hasChild(teamID)) error = true
                else {
                    val team = data.child(teamID).getValue(Team::class.java)
                    if (team != null && team.id == teamID) {
                        listener.teamUpdate(team, this)
                    } else error = true
                }
                if(error) {
                    //listener.dataError(R.string.team_not_found)
                    Log.i("FirebaseDao.getTeam", "team requested but not found")
                }
            }
        })
    }

    override fun deleteTeam(teamID: String) {
        val table = database.getReference(TEAM_TABLE)
        table.child(teamID).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(data: DataSnapshot) {
                val team = data.getValue(Team::class.java)
                if(team != null && team.id == teamID) {
                    for(playerID in team.playerIDs) {
                        deletePlayer(playerID)
                    }
                }
            }
            override fun onCancelled(e: DatabaseError) { logError(e) }
        })
        table.child(teamID).removeValue()
    }

    override fun updateTeam(team: Team) {
        val table = database.getReference(TEAM_TABLE)
        table.child(team.id).setValue(team)
    }
    
    //players

    override fun addPlayer(player: Player) {
        val table = database.getReference(PLAYER_TABLE)
        table.child(player.id).setValue(player)
    }

    override fun getPlayers(listener: ILiveDatabase.Listener, playerIDs: List<String>) {
        val table = database.getReference(PLAYER_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                val players = LinkedList<Player>()
                var playerMissing = false
                for(playerID in playerIDs) {
                    var error = false
                    if(!data.hasChild(playerID)) error = true
                    else {
                        val player = data.child(playerID).getValue(Player::class.java)
                        if (player != null && player.id == playerID) {
                            players.add(player)
                        } else error = true
                    }
                    if(error) playerMissing = true
                }
                if(playerMissing) {
                    //listener.dataError(R.string.player_not_found)
                    Log.i("FirebaseDao.getPlayers", "player requested but not found")
                }
                listener.playersUpdate(players, this)
            }
        })
    }

    override fun deletePlayer(playerID: String) {
        val table = database.getReference(PLAYER_TABLE)
        table.child(playerID).removeValue()
    }

    override fun updatePlayer(player: Player) {
        val table = database.getReference(PLAYER_TABLE)
        table.child(player.id).setValue(player)
    }
    
    //matches

    override fun addMatch(match: Match) {
        val table = database.getReference(MATCH_TABLE)
        table.child(match.id).setValue(match)
    }

    override fun addMatches(matches: List<Match>) {
        val table = database.getReference(MATCH_TABLE)
        for(match in matches) table.child(match.id).setValue(match)
    }

    override fun getMatches(listener: ILiveDatabase.Listener, matchIDs: List<String>) {
        val table = database.getReference(MATCH_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                val matches = LinkedList<Match>()
                var matchMissing = false
                for(matchID in matchIDs) {
                    var error = false
                    if(!data.hasChild(matchID)) error = true
                    else {
                        val match = data.child(matchID).getValue(Match::class.java)
                        if (match != null && match.id == matchID) {
                            matches.add(match)
                        } else error = true
                    }
                    if(error) matchMissing = true
                }
                if(matchMissing) {
                    //listener.dataError(R.string.match_not_found)
                    Log.i("FirebaseDao.getMatches", "match requested but not found")
                }
                listener.matchesUpdate(matches, this)
            }
        })
    }

    override fun getMatch(listener: ILiveDatabase.Listener, matchID: String) {
        val table = database.getReference(MATCH_TABLE)
        table.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(e: DatabaseError) {
                logError(e)
                listener.dataError(R.string.database_error)
            }
            override fun onDataChange(data: DataSnapshot) {
                var error = false
                if(!data.hasChild(matchID)) error = true
                else {
                    val match = data.child(matchID).getValue(Match::class.java)
                    if (match != null && match.id == matchID) {
                        listener.matchUpdate(match, this)
                    } else error = true
                }
                if(error) {
                    //listener.dataError(R.string.match_not_found)
                    Log.i("FirebaseDao.getMatch", "match requested but not found")
                }
            }
        })
    }

    override fun deleteMatch(matchID: String) {
        val table = database.getReference(MATCH_TABLE)
        table.child(matchID).removeValue()
    }

    override fun updateMatch(match: Match) {
        val table = database.getReference(MATCH_TABLE)
        table.child(match.id).setValue(match)
    }
}