package ss.virovitica.sportsballtourney.database

import android.util.Log
import androidx.annotation.StringRes
import com.google.firebase.database.ValueEventListener
import ss.virovitica.sportsballtourney.model.*

interface ILiveDatabase {
    fun removeListener(listener: ValueEventListener)

    fun saveUser(user: User)
    fun getAllUsers(listener: Listener)
    fun getUser(userID: String, listener: Listener)

    fun addTournament(tournament: Tournament)
    fun getAllTournaments(listener: Listener)
    fun getTournament(listener: Listener, tournamentID: String)
    fun deleteTournament(tournamentID: String)
    fun updateTournament(tournament: Tournament)

    fun addTeam(team: Team)
    fun getTeams(listener: Listener, teamIDs: List<String>)
    fun getTeam(listener: Listener, teamID: String)
    fun deleteTeam(teamID: String)
    fun updateTeam(team: Team)

    fun addPlayer(player: Player)
    fun getPlayers(listener: Listener, playerIDs: List<String>)
    fun deletePlayer(playerID: String)
    fun updatePlayer(player: Player)

    fun addMatch(match: Match)
    fun addMatches(matches: List<Match>)
    fun getMatches(listener: Listener, matchIDs: List<String>)
    fun getMatch(listener: Listener, matchID: String)
    fun deleteMatch(matchID: String)
    fun updateMatch(match: Match)

    interface Listener {
        fun usersAllUpdate(users: List<User>, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "usersAllUpdate() not implemented, but called")
        }
        fun userUpdate(user: User, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "userUpdate() not implemented, but called")
        }
        fun tournamentsAllUpdate(tournaments: List<Tournament>, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "tournamentsAllUpdate() not implemented, but called")
        }
        fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "tournamentUpdate() not implemented, but called")
        }
        fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "teamsUpdate() not implemented, but called")
        }
        fun teamUpdate(team: Team, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "teamUpdate() not implemented, but called")
        }
        fun playersUpdate(players: List<Player>, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "playersUpdate() not implemented, but called")
        }
        fun matchesUpdate(matches: List<Match>, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "matchesUpdate() not implemented, but called")
        }
        fun matchUpdate(match: Match, listener: ValueEventListener) {
            Log.e("ILiveDatabase.Listener", "matchUpdate() not implemented, but called")
        }
        fun dataError(@StringRes messageID: Int)
    }
}