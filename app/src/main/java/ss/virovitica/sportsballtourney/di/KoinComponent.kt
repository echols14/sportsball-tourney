package ss.virovitica.sportsballtourney.di

import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.experimental.builder.singleBy
import ss.virovitica.sportsballtourney.activity.editplayers.presenter.EditPlayersPresenter
import ss.virovitica.sportsballtourney.activity.editplayers.presenter.IEditPlayersPresenter
import ss.virovitica.sportsballtourney.activity.editplayers.view.IEditPlayersView
import ss.virovitica.sportsballtourney.activity.editteams.presenter.EditTeamsPresenter
import ss.virovitica.sportsballtourney.activity.editteams.presenter.IEditTeamsPresenter
import ss.virovitica.sportsballtourney.activity.editteams.view.IEditTeamsView
import ss.virovitica.sportsballtourney.activity.login.presenter.ILoginPresenter
import ss.virovitica.sportsballtourney.activity.login.presenter.LoginPresenter
import ss.virovitica.sportsballtourney.activity.login.view.ILoginView
import ss.virovitica.sportsballtourney.activity.main.presenter.IMainPresenter
import ss.virovitica.sportsballtourney.activity.main.presenter.MainPresenter
import ss.virovitica.sportsballtourney.activity.main.view.IMainView
import ss.virovitica.sportsballtourney.activity.team.presenter.ITeamPresenter
import ss.virovitica.sportsballtourney.activity.team.presenter.TeamPresenter
import ss.virovitica.sportsballtourney.activity.team.view.ITeamView
import ss.virovitica.sportsballtourney.activity.tournaments.presenter.ITournamentsPresenter
import ss.virovitica.sportsballtourney.activity.tournaments.presenter.TournamentsPresenter
import ss.virovitica.sportsballtourney.activity.tournaments.view.ITournamentsView
import ss.virovitica.sportsballtourney.database.FirebaseDao
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.fragment.bottomsheet.presenter.IMatchSheetPresenter
import ss.virovitica.sportsballtourney.fragment.bottomsheet.presenter.MatchSheetPresenter
import ss.virovitica.sportsballtourney.fragment.bottomsheet.view.IMatchSheetView
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.BracketPresenter
import ss.virovitica.sportsballtourney.fragment.matches.bracket.presenter.IBracketPresenter
import ss.virovitica.sportsballtourney.fragment.matches.bracket.view.IBracketView
import ss.virovitica.sportsballtourney.fragment.matches.groups.presenter.GroupsPresenter
import ss.virovitica.sportsballtourney.fragment.matches.groups.presenter.IGroupsPresenter
import ss.virovitica.sportsballtourney.fragment.matches.groups.view.IGroupsView
import ss.virovitica.sportsballtourney.fragment.teams.presenter.ITeamsPresenter
import ss.virovitica.sportsballtourney.fragment.teams.presenter.TeamsPresenter
import ss.virovitica.sportsballtourney.fragment.teams.view.ITeamsView
import ss.virovitica.sportsballtourney.fragment.tournamentinfo.presenter.ITournamentInfoPresenter
import ss.virovitica.sportsballtourney.fragment.tournamentinfo.presenter.TournamentInfoPresenter
import ss.virovitica.sportsballtourney.fragment.tournamentinfo.view.ITournamentInfoView

val authModule = module {
    single { AuthUI.getInstance() }
    single { FirebaseAuth.getInstance() }
}

val databaseModule = module {
    singleBy<ILiveDatabase, FirebaseDao>()
    single{ FirebaseDao() }
}

val activityModule = module {
    factory<ILoginPresenter> { (view: ILoginView) -> LoginPresenter(view, get(), get(), get()) }
    factory<ITournamentsPresenter> { (view: ITournamentsView) -> TournamentsPresenter(view, get(), get()) }
    factory<IMainPresenter> { (view: IMainView) -> MainPresenter(view, get(), get()) }
    factory<IEditTeamsPresenter> { (view: IEditTeamsView) -> EditTeamsPresenter(view, get()) }
    factory<IEditPlayersPresenter> { (view: IEditPlayersView) -> EditPlayersPresenter(view, get()) }
    factory<ITeamPresenter> { (view: ITeamView) -> TeamPresenter(view, get()) }
}

val fragmentModule = module {
    factory<ITournamentInfoPresenter> { (view: ITournamentInfoView) -> TournamentInfoPresenter(view, get(), get()) }
    factory<IGroupsPresenter> { (view: IGroupsView) -> GroupsPresenter(view, get(), get()) }
    factory<IBracketPresenter> { (view: IBracketView) -> BracketPresenter(view, get(), get()) }
    factory<IMatchSheetPresenter> { (view: IMatchSheetView) -> MatchSheetPresenter(view, get()) }
    factory<ITeamsPresenter> { (view: ITeamsView) -> TeamsPresenter(view, get()) }
}

//component
val appComponent: List<Module> = listOf(authModule, databaseModule, activityModule, fragmentModule)
