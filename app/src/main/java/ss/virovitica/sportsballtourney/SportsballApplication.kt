package ss.virovitica.sportsballtourney

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ss.virovitica.sportsballtourney.di.appComponent

class SportsballApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SportsballApplication)
            modules(appComponent)
        }
    }
}