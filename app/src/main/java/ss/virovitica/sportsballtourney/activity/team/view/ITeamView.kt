package ss.virovitica.sportsballtourney.activity.team.view

import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface ITeamView: IView {
    fun setTitle(title: String)
    fun setPlayers(players: List<Player>)
    fun setTeamStats(forPoints: Int, againstPoints: Int)
    fun setMetaPoints(metaPoints: Int)
}