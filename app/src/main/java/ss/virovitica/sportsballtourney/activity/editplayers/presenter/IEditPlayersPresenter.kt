package ss.virovitica.sportsballtourney.activity.editplayers.presenter

import ss.virovitica.sportsballtourney.model.Player

interface IEditPlayersPresenter {
    val teamFull: Boolean
    fun fetchTeam(teamID: String)
    fun fetchPlayers(playerIDs: List<String>)
    fun addPlayer(newPlayer: Player)
    fun updatePlayer(player: Player)
    fun deletePlayer(playerID: String)
    fun unsubscribe()
}