package ss.virovitica.sportsballtourney.activity.main.view

import android.content.Context
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.FragmentManager
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.main.presenter.IMainPresenter
import ss.virovitica.sportsballtourney.activity.tournaments.view.TournamentsActivity
import ss.virovitica.sportsballtourney.fragment.matches.MatchesFragment
import ss.virovitica.sportsballtourney.fragment.teams.view.TeamsFragment
import ss.virovitica.sportsballtourney.fragment.tournamentinfo.view.TournamentInfoFragment
import ss.virovitica.sportsballtourney.model.User
import ss.virovitica.sportsballtourney.utility.`interface`.LogoutManager
import ss.virovitica.sportsballtourney.utility.switchToFragment

class MainActivity : AppCompatActivity(), IMainView, NavigationView.OnNavigationItemSelectedListener, LogoutManager {
    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.activity.main.view.MainActivity.KEY_TOURNAMENT_ID"
    }

    //members
    override val ctx: Context
        get() = this
    private val presenter: IMainPresenter by inject { parametersOf(this) }
    override val context: Context
        get() = this
    override val fragmentManager: FragmentManager
        get() = supportFragmentManager
    override val authUI: AuthUI by inject()
    override val auth: FirebaseAuth by inject()

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //get info from the intent
        val tournamentID = intent.getStringExtra(KEY_TOURNAMENT_ID)
        presenter.setTournament(tournamentID)
        //set up the toolbar
        setSupportActionBar(mainToolbar)
        //set up the drawer
        val toggle = ActionBarDrawerToggle(this, drawerLayout, mainToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
        //load user data into the drawer header
        presenter.getUserDetails()
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_logout -> {
                attemptLogout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //***IMainView***//

    override fun setToolbarTitle(title: String) {
        mainToolbar?.title = title
    }

    override fun goToFirstTab() { //switch to the first fragment
        if(presenter.getTournament()?.started == true) {
            navView?.setCheckedItem(R.id.nav_matches)
            navView?.menu?.performIdentifierAction(R.id.nav_matches, 0)
        }
        else {
            navView?.setCheckedItem(R.id.nav_tourney_details)
            navView?.menu?.performIdentifierAction(R.id.nav_tourney_details, 0)
        }
    }

    override fun setUserDetails(user: User) {
        navView.getHeaderView(0).usernameText.text =
            if(user.name == User.ANONYMOUS) getString(R.string.guest)
            else user.name
        navView.getHeaderView(0).usernameDetailText.text = user.detail
        navView.getHeaderView(0).usernameDetailText.visibility =
            if(user.detail == "") GONE
            else VISIBLE
    }

    override fun notifyOfDelete() {
        presenter.notifyOfDelete()
    }

    override fun back() = onBackPressed()

    //***OnNavigationItemSelectedListener***//

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val tournamentID = presenter.getTournament()?.id ?: ""
        when (item.itemId) {
            R.id.nav_matches -> {
                val newFragment = MatchesFragment()
                if(tournamentID != "") {
                    val args = Bundle()
                    val groupsFinished = presenter.getTournament()?.bracketStarted ?: false
                    args.putBoolean(MatchesFragment.KEY_GROUPS_FINISHED, groupsFinished)
                    args.putString(MatchesFragment.KEY_TOURNAMENT_ID, tournamentID)
                    newFragment.arguments = args
                }
                switchToFragment(supportFragmentManager, R.id.fragmentHolder, newFragment)
            }
            R.id.nav_teams -> {
                val newFragment = TeamsFragment()
                if(tournamentID != "") {
                    val args = Bundle()
                    args.putString(TeamsFragment.KEY_TOURNAMENT_ID, tournamentID)
                    newFragment.arguments = args
                }
                switchToFragment(supportFragmentManager, R.id.fragmentHolder, newFragment)
            }
            R.id.nav_tourney_details -> {
                val newFragment = TournamentInfoFragment()
                if(tournamentID != "") {
                    val args = Bundle()
                    args.putString(TournamentInfoFragment.KEY_TOURNAMENT_ID, tournamentID)
                    newFragment.arguments = args
                }
                switchToFragment(supportFragmentManager, R.id.fragmentHolder, newFragment)
            }
            R.id.nav_all_tournaments -> { startActivity(intentFor<TournamentsActivity>().clearTop()) }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
