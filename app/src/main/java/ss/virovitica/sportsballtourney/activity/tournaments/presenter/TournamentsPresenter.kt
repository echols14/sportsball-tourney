package ss.virovitica.sportsballtourney.activity.tournaments.presenter

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.tournaments.view.ITournamentsView
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.detail
import ss.virovitica.sportsballtourney.utility.safeDisplayName
import java.util.*
import kotlin.collections.ArrayList

class TournamentsPresenter(private val view: ITournamentsView, private val database: ILiveDatabase, private val auth: FirebaseAuth):
    ITournamentsPresenter, ILiveDatabase.Listener {

    //members
    override val loggedIn
        get() = (auth.currentUser != null)
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private var tournaments: List<Tournament> = ArrayList()

    //functions

    //***ITournamentsPresenter***//

    override fun fetchTournaments() {
        database.getAllTournaments(this)
    }

    override fun filterTournaments(query: String) {
        if(query == "") { //no query, show all
            view.setTournaments(tournaments)
            return
        }
        //find tournaments that have the query in their name
        val filteredTournaments: MutableList<Tournament> = ArrayList()
        for(tournament in tournaments) {
            if(tournament.name.contains(query, true)) filteredTournaments.add(tournament)
        }
        view.setTournaments(filteredTournaments)
    }

    override fun makeTournament(tourneyName: String) {
        val user = auth.currentUser
        if(user == null) {
            Log.e("TournamentsPresenter", "Unauthorized tournament creation attempt")
            view.ctx.toast(R.string.please_login)
            return
        }
        val newTournament = Tournament(tourneyName)
        newTournament.addManager(user.uid, user.safeDisplayName, user.detail)
        database.addTournament(newTournament)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentsAllUpdate(tournaments: List<Tournament>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.tournaments = tournaments
        view.setTournaments(tournaments)
    }

    override fun dataError(messageID: Int) {
        Log.i("TournamentsPresenter", "dataError called")
        view.ctx.toast(messageID)
    }
}