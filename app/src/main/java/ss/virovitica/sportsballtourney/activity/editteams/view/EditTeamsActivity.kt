package ss.virovitica.sportsballtourney.activity.editteams.view

import android.content.Context
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_simple_recycler.*
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editteams.presenter.IEditTeamsPresenter
import ss.virovitica.sportsballtourney.adapter.EditTeamRecyclerAdapter
import ss.virovitica.sportsballtourney.dialog.TeamDialog
import ss.virovitica.sportsballtourney.dialog.TeamDialog.Companion.TEAM_DIALOG
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.utility.`interface`.ToolbarManager

class EditTeamsActivity: AppCompatActivity(), IEditTeamsView, ToolbarManager, TeamDialog.Listener {
    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.activity.editteams.view.EditTeamsActivity.KEY_TOURNAMENT_ID"
    }

    //members
    override val ctx: Context
        get() = this
    override val toolbar by lazy { find<Toolbar>(R.id.mainToolbar) }
    private val presenter: IEditTeamsPresenter by inject { parametersOf(this) }
    private val adapter = EditTeamRecyclerAdapter(this)

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_recycler)
        //set up the toolbar
        initToolbarEditTeams()
        enableHomeAsUp { onBackPressed() }
        //get info from the intent
        val tournamentID = intent.getStringExtra(KEY_TOURNAMENT_ID)
        presenter.fetchTournament(tournamentID)
        //set up the recycler
        simpleRecycler.layoutManager = LinearLayoutManager(this)
        simpleRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        simpleRecycler.adapter = adapter
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    //***IEditTeamsView***//

    override fun setTitle(title: String) {
        toolbarTitle = title
    }

    override fun setTeams(teams: List<Team>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(teams.isEmpty()) {
            simpleRecycler?.visibility = GONE
            emptyView?.visibility = VISIBLE
            emptyViewText?.text = getString(R.string.no_teams)
        }
        else simpleRecycler?.visibility = VISIBLE
        //put the data in
        adapter.setTeams(teams)
    }

    override fun editDialog(team: Team) {
        TeamDialog(this, team, !presenter.tournamentStarted).show(supportFragmentManager, TEAM_DIALOG)
    }

    //***ToolbarManager***//

    override fun newTeam() {
        if(!presenter.tournamentFull) TeamDialog(this, null, !presenter.tournamentStarted).show(supportFragmentManager, TEAM_DIALOG)
        else toast(R.string.tournament_full)
    }

    //***TeamDialog.Listener***//

    override fun onSubmit(team: Team, isEdit: Boolean) {
        if(isEdit) presenter.updateTeam(team)
        else presenter.addTeam(team)
    }

    override fun onDelete(teamID: String) {
        presenter.deleteTeam(teamID)
    }
}