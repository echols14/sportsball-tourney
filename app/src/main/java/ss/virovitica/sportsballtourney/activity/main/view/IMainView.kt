package ss.virovitica.sportsballtourney.activity.main.view

import ss.virovitica.sportsballtourney.model.User
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface IMainView: IView {
    fun setToolbarTitle(title: String)
    fun goToFirstTab()
    fun setUserDetails(user: User)
    fun notifyOfDelete()
    fun back()
}