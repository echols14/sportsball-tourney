package ss.virovitica.sportsballtourney.activity.editplayers.view

import android.content.Context
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_simple_recycler.*
import kotlinx.android.synthetic.main.activity_simple_recycler.emptyView
import kotlinx.android.synthetic.main.activity_simple_recycler.progressView
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editplayers.presenter.IEditPlayersPresenter
import ss.virovitica.sportsballtourney.adapter.EditPlayerRecyclerAdapter
import ss.virovitica.sportsballtourney.dialog.PlayerDialog
import ss.virovitica.sportsballtourney.dialog.PlayerDialog.Companion.PLAYER_DIALOG
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.utility.`interface`.ToolbarManager

class EditPlayersActivity: AppCompatActivity(), IEditPlayersView, ToolbarManager, PlayerDialog.Listener {
    companion object {
        const val KEY_TEAM_ID = "ss.virovitica.sportsballtourney.activity.editteams.view.EditTeamsActivity.KEY_TOURNAMENT_ID"
        const val KEY_IS_OWNER = "ss.virovitica.sportsballtourney.activity.editteams.view.EditTeamsActivity.KEY_IS_OWNER"
    }

    //members
    override val ctx: Context
        get() = this
    override val toolbar by lazy { find<Toolbar>(R.id.mainToolbar) }
    private val presenter: IEditPlayersPresenter by inject { parametersOf(this) }
    private lateinit var adapter: EditPlayerRecyclerAdapter
    private var isOwner = false

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_recycler)
        //get info from the intent
        val teamID = intent.getStringExtra(KEY_TEAM_ID)
        presenter.fetchTeam(teamID)
        isOwner = intent.getBooleanExtra(KEY_IS_OWNER, false)
        //set up the toolbar
        if(isOwner) initToolbarEditPlayers()
        enableHomeAsUp { onBackPressed() }
        //set up the recycler
        adapter = EditPlayerRecyclerAdapter(this, isOwner)
        simpleRecycler.layoutManager = LinearLayoutManager(this)
        simpleRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        simpleRecycler.adapter = adapter
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    //***IEditTeamsView***//

    override fun setTitle(title: String) {
        toolbarTitle = title
    }

    override fun setPlayers(players: List<Player>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(players.isEmpty()) {
            simpleRecycler?.visibility = GONE
            emptyView?.visibility = VISIBLE
            emptyViewText?.text = getString(R.string.no_players)
        }
        else simpleRecycler?.visibility = VISIBLE
        //put the data in
        adapter.setPlayers(players)
    }

    override fun editDialog(player: Player) {
        PlayerDialog(this, player).show(supportFragmentManager, PLAYER_DIALOG)
    }

    //***ToolbarManager***//

    override fun newPlayer() {
        if(!presenter.teamFull) PlayerDialog(this, null).show(supportFragmentManager, PLAYER_DIALOG)
        else toast(R.string.team_full)
    }

    //***PlayerDialog.Listener***//

    override fun onSubmit(player: Player, isEdit: Boolean) {
        if(isEdit) presenter.updatePlayer(player)
        else presenter.addPlayer(player)
    }

    override fun onDelete(playerID: String) {
        presenter.deletePlayer(playerID)
    }
}