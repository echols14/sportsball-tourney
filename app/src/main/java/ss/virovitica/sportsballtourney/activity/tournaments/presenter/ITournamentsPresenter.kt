package ss.virovitica.sportsballtourney.activity.tournaments.presenter

interface ITournamentsPresenter {
    val loggedIn: Boolean
    fun fetchTournaments()
    fun filterTournaments(query: String)
    fun makeTournament(tourneyName: String)
    fun unsubscribe()
}