package ss.virovitica.sportsballtourney.activity.tournaments.view

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_tournaments.*
import org.jetbrains.anko.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.tournaments.presenter.ITournamentsPresenter
import ss.virovitica.sportsballtourney.adapter.TournamentRecyclerAdapter
import ss.virovitica.sportsballtourney.dialog.LogoutDialog
import ss.virovitica.sportsballtourney.dialog.NewTournamentDialog
import ss.virovitica.sportsballtourney.dialog.NewTournamentDialog.Companion.NEW_TOURNAMENT_DIALOG
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.`interface`.LogoutManager
import ss.virovitica.sportsballtourney.utility.`interface`.ToolbarManager

class TournamentsActivity: AppCompatActivity(), ITournamentsView, LogoutDialog.Listener, NewTournamentDialog.Listener, ToolbarManager, LogoutManager {
    //members
    override val ctx: Context
        get() = this
    override val toolbar by lazy { find<Toolbar>(R.id.mainToolbar) }
    private val presenter: ITournamentsPresenter by inject { parametersOf(this) }
    override val context: Context
        get() = this
    override val fragmentManager: FragmentManager
        get() = supportFragmentManager
    override val authUI: AuthUI by inject()
    override val auth: FirebaseAuth by inject()
    private val adapter = TournamentRecyclerAdapter(auth.currentUser?.uid ?: "")

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tournaments)
        //set up the toolbar
        initToolbarTournaments(presenter.loggedIn)
        toolbarTitle = getString(R.string.all_tournaments)
        //set up the RecyclerView
        tournamentRecycler.layoutManager = LinearLayoutManager(this)
        tournamentRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        tournamentRecycler.adapter = adapter
        //set up the search bar
        searchQueryInput.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(s != null) presenter.filterTournaments(s.toString())
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        //set up the clear query button
        clearButton.setOnClickListener {
            searchQueryInput.text.clear()
        }
        //pull tournaments from the database
        presenter.fetchTournaments()
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun onBackPressed() { attemptLogout() }

    //***ITournamentsView***//

    override fun setTournaments(tournaments: List<Tournament>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(tournaments.isEmpty()) {
            tournamentRecycler?.visibility = GONE
            emptyView?.visibility = VISIBLE
        }
        else tournamentRecycler?.visibility = VISIBLE
        //put the data in
        adapter.setTournaments(tournaments)
    }

    //***NewTournamentDialog.Listener***//

    override fun onSubmit(tourneyName: String) {
        presenter.makeTournament(tourneyName)
    }

    //***ToolbarManager***//

    override fun logout() { attemptLogout() }

    override fun newTournament() {
        NewTournamentDialog(this).show(supportFragmentManager, NEW_TOURNAMENT_DIALOG)
    }
}