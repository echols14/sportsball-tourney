package ss.virovitica.sportsballtourney.activity.login.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.login.presenter.ILoginPresenter
import ss.virovitica.sportsballtourney.activity.tournaments.view.TournamentsActivity
import ss.virovitica.sportsballtourney.utility.`interface`.ToolbarManager

class LoginActivity: AppCompatActivity(), ILoginView, ToolbarManager {
    companion object {
        private const val RC_SIGN_IN = 42
    }
    //members
    override val ctx: Context
        get() = this
    override val toolbar by lazy { find<Toolbar>(R.id.mainToolbar) }
    private val presenter: ILoginPresenter by inject { parametersOf(this) }

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //set up the toolbar
        toolbarTitle = getString(R.string.app_name)
        //set up the buttons
        loginButton.setOnClickListener { //opens firebase login/register screen
            startActivityForResult(presenter.buildLoginIntent(), RC_SIGN_IN)
        }
        guestButton.setOnClickListener { startActivity<TournamentsActivity>() }
    }

    override fun onResume() {
        super.onResume()
        if(presenter.loggedIn) startActivity<TournamentsActivity>()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) { //firebase login/register ended
            if (resultCode == Activity.RESULT_OK) { //successfully signed in
                presenter.saveUser()
                startActivity<TournamentsActivity>()
            }
            /* else: Sign in failed.
            If response is null the user canceled the sign-in flow using the back button.
            Otherwise check response.getError().getErrorCode() and handle the error.*/
            //val response = IdpResponse.fromResultIntent(data)
        }
        else { super.onActivityResult(requestCode, resultCode, data) }
    }
}