package ss.virovitica.sportsballtourney.activity.editteams.presenter

import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editteams.view.IEditTeamsView
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.model.Tournament
import java.util.*
import kotlin.collections.ArrayList

class EditTeamsPresenter(private val view: IEditTeamsView, private val database: ILiveDatabase): IEditTeamsPresenter, ILiveDatabase.Listener {
    //members
    private lateinit var tournament: Tournament
    private val teams: MutableList<Team> = ArrayList(Team.MAX_TEAM_SIZE)
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    override val tournamentFull: Boolean
        get() = (tournamentReceived && tournament.ready)
    override val tournamentStarted: Boolean
        get() = (tournamentReceived && tournament.started)
    private var tournamentReceived = false
    private var deleting = false

    //functions

    //***IEditTeamsPresenter***//

    override fun fetchTournament(tournamentID: String) {
        database.getTournament(this, tournamentID)
    }

    override fun fetchTeams(teamIDs: List<String>) {
        database.getTeams(this, teamIDs)
    }

    override fun addTeam(newTeam: Team) {
        if(!tournament.addTeam(newTeam.id)) {
            view.ctx.toast(R.string.team_add_failed)
            return
        }
        database.addTeam(newTeam)
        database.updateTournament(tournament)
    }

    override fun updateTeam(team: Team) {
        database.updateTeam(team)
    }

    override fun deleteTeam(teamID: String) {
        deleting = true
        tournament.teamIDs.remove(teamID)
        database.updateTournament(tournament)
        database.deleteTeam(teamID)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        tournamentReceived = true
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.tournament = tournament
        fetchTeams(tournament.teamIDs)
        view.setTitle(tournament.name)
    }

    override fun teamsUpdate(teams: List<Team>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.teams.clear()
        this.teams.addAll(teams)
        view.setTeams(teams)
    }

    override fun dataError(messageID: Int) {
        if (deleting) deleting = false
        else view.ctx.toast(messageID)
    }
}