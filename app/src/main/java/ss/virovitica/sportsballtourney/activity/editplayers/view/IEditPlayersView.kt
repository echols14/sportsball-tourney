package ss.virovitica.sportsballtourney.activity.editplayers.view

import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface IEditPlayersView: IView {
    fun setTitle(title: String)
    fun setPlayers(players: List<Player>)
    fun editDialog(player: Player)
}