package ss.virovitica.sportsballtourney.activity.team.view

import android.content.Context
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_team.*
import kotlinx.android.synthetic.main.activity_team.emptyView
import kotlinx.android.synthetic.main.activity_team.progressView
import org.jetbrains.anko.find
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.team.presenter.ITeamPresenter
import ss.virovitica.sportsballtourney.adapter.PlayerDetailRecyclerAdapter
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.utility.`interface`.ToolbarManager

class TeamActivity: AppCompatActivity(), ITeamView, ToolbarManager {
    companion object {
        const val KEY_TOURNAMENT_ID = "ss.virovitica.sportsballtourney.activity.team.view.TeamActivity.KEY_TOURNAMENT_ID"
        const val KEY_TEAM_ID = "ss.virovitica.sportsballtourney.activity.team.view.TeamActivity.KEY_TEAM_ID"
    }

    //members
    override val ctx: Context
        get() = this
    override val toolbar by lazy { find<Toolbar>(R.id.mainToolbar) }
    private val presenter: ITeamPresenter by inject{ parametersOf(this) }
    private val adapter = PlayerDetailRecyclerAdapter()

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team)
        //get info from the intent
        val tournamentID = intent.getStringExtra(KEY_TOURNAMENT_ID) ?: ""
        val teamID = intent.getStringExtra(KEY_TEAM_ID) ?: ""
        //get stuff from the database
        presenter.setTournamentID(tournamentID)
        presenter.fetchTeam(teamID)
        //set up the toolbar
        enableHomeAsUp { onBackPressed() }
        //set up the recycler
        simpleRecycler.layoutManager = LinearLayoutManager(this)
        simpleRecycler.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        simpleRecycler.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribe()
    }

    //***ITeamView***//

    override fun setTitle(title: String) { toolbarTitle = title }

    override fun setPlayers(players: List<Player>) {
        //hide the loading bar and show the list
        progressView?.visibility = GONE
        if(players.isEmpty()) {
            simpleRecycler?.visibility = GONE
            emptyView?.visibility = VISIBLE
        }
        else simpleRecycler?.visibility = VISIBLE
        //put the data in
        adapter.setPlayers(players)
    }

    override fun setTeamStats(forPoints: Int, againstPoints: Int) {
        pointsForText?.text = forPoints.toString()
        pointsAgainstText?.text = againstPoints.toString()
        val difference = forPoints - againstPoints
        pointsDifferenceText?.text =
            if(difference > 0) "+$difference"
            else difference.toString()
    }

    override fun setMetaPoints(metaPoints: Int) { groupPointsText?.text = metaPoints.toString() }
}