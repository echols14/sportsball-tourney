package ss.virovitica.sportsballtourney.activity.team.presenter

interface ITeamPresenter {
    fun setTournamentID(tournamentID: String)
    fun fetchTeam(teamID: String)
    fun unsubscribe()
}