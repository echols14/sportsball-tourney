package ss.virovitica.sportsballtourney.activity.login.presenter

import android.content.Intent

interface ILoginPresenter {
    val loggedIn: Boolean
    fun buildLoginIntent(): Intent
    fun saveUser()
}