package ss.virovitica.sportsballtourney.activity.editteams.presenter

import ss.virovitica.sportsballtourney.model.Team

interface IEditTeamsPresenter {
    val tournamentFull: Boolean
    val tournamentStarted: Boolean
    fun fetchTournament(tournamentID: String)
    fun fetchTeams(teamIDs: List<String>)
    fun addTeam(newTeam: Team)
    fun updateTeam(team: Team)
    fun deleteTeam(teamID: String)
    fun unsubscribe()
}