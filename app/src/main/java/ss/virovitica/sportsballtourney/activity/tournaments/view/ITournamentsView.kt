package ss.virovitica.sportsballtourney.activity.tournaments.view

import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface ITournamentsView: IView {
    fun setTournaments(tournaments: List<Tournament>)
}