package ss.virovitica.sportsballtourney.activity.editplayers.presenter

import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.editplayers.view.IEditPlayersView
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.Player
import ss.virovitica.sportsballtourney.model.Team
import java.util.*
import kotlin.collections.ArrayList

class EditPlayersPresenter(private val view: IEditPlayersView, private val database: ILiveDatabase): IEditPlayersPresenter, ILiveDatabase.Listener {
    //members
    private lateinit var team: Team
    private val players: MutableList<Player> = ArrayList(Team.MAX_TEAM_SIZE)
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    override val teamFull: Boolean
        get() = (teamReceived && team.full)
    private var teamReceived = false
    private var deleting = false

    //functions

    //***IEditPlayersPresenter***//

    override fun fetchTeam(teamID: String) {
        database.getTeam(this, teamID)
    }

    override fun fetchPlayers(playerIDs: List<String>) {
        database.getPlayers(this, playerIDs)
    }

    override fun addPlayer(newPlayer: Player) {
        if(!team.addPlayer(newPlayer)) {
            view.ctx.toast(R.string.player_add_failed)
            return
        }
        database.addPlayer(newPlayer)
        database.updateTeam(team)
    }

    override fun updatePlayer(player: Player) {
        database.updatePlayer(player)
    }

    override fun deletePlayer(playerID: String) {
        deleting = true
        team.playerIDs.remove(playerID)
        database.updateTeam(team)
        database.deletePlayer(playerID)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun teamUpdate(team: Team, listener: ValueEventListener) {
        teamReceived = true
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.team = team
        fetchPlayers(team.playerIDs)
        view.setTitle(team.name)
    }

    override fun playersUpdate(players: List<Player>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.players.clear()
        this.players.addAll(players)
        view.setPlayers(players)
    }

    override fun dataError(messageID: Int) {
        if(deleting) deleting = false
        else view.ctx.toast(messageID)
    }
}