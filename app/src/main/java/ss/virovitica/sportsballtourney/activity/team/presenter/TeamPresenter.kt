package ss.virovitica.sportsballtourney.activity.team.presenter

import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.activity.team.view.ITeamView
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.*
import ss.virovitica.sportsballtourney.utility.countMetaPoints
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TeamPresenter(private val view: ITeamView, private val database: ILiveDatabase): ITeamPresenter, ILiveDatabase.Listener {
    //members
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private var tournamentID: String = ""
    private var team: Team? = null
    private var group: Group? = null

    //functions

    //***ITeamPresenter***//

    override fun setTournamentID(tournamentID: String) {
        this.tournamentID = tournamentID
    }

    override fun fetchTeam(teamID: String) {
        database.getTeam(this, teamID)
    }

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        val teamID = team?.id ?: return
        groupSearch@ for(group in tournament.getGroups()) {
            if(!group.getTeamIDs().contains(teamID)) continue
            for(tempTeamID in group.getTeamIDs()) {
                if(tempTeamID == teamID) {
                    this.group = group
                    val allMatchIDs: MutableList<String> = ArrayList(group.getMatchIDs().size + tournament.getBracketMatchIDs().size)
                    allMatchIDs.addAll(group.getMatchIDs())
                    allMatchIDs.addAll(tournament.getBracketMatchIDs())
                    database.getMatches(this, allMatchIDs)
                    break@groupSearch
                }
            }
        }
    }

    override fun matchesUpdate(matches: List<Match>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        val teamID = team?.id ?: return
        //figure this team's meta-points from group play
        val thisGroup = group
        if(thisGroup != null) {
            val matchesMap: MutableMap<String, Match> = HashMap()
            for(match in matches) matchesMap[match.id] = match
            val metaPointsMap = countMetaPoints(thisGroup, matchesMap)
            val metaPoints = metaPointsMap[teamID]
            if(metaPoints != null) {
                view.setMetaPoints(metaPoints)
            }
        }
        //figure teams stats
        var totalForPoints = 0
        var totalAgainstPoints = 0
        for(match in matches) {
            if(match.teamIdA == teamID) {
                totalForPoints += match.scoreTeamA
                totalAgainstPoints += match.scoreTeamB
            }
            else if(match.teamIdB == teamID) {
                totalForPoints += match.scoreTeamB
                totalAgainstPoints += match.scoreTeamA
            }
            //else continue
        }
        view.setTeamStats(totalForPoints, totalAgainstPoints)
    }

    override fun teamUpdate(team: Team, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        this.team = team
        database.getPlayers(this, team.playerIDs)
        view.setTitle(team.name)
        //get the tournament so we can get the group's stats
        database.getTournament(this, tournamentID)
    }

    override fun playersUpdate(players: List<Player>, listener: ValueEventListener) {
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        view.setPlayers(players)
    }

    override fun dataError(messageID: Int) {
        view.ctx.toast(messageID)
    }
}