package ss.virovitica.sportsballtourney.activity.login.presenter

import android.content.Intent
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.login.view.ILoginView
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.User
import ss.virovitica.sportsballtourney.utility.detail
import ss.virovitica.sportsballtourney.utility.safeDisplayName

class LoginPresenter(private val view: ILoginView, private val database: ILiveDatabase, private val authUI: AuthUI, private val auth: FirebaseAuth): ILoginPresenter {
    //members
    override val loggedIn
        get() = (auth.currentUser != null)

    //functions

    override fun buildLoginIntent(): Intent {
        //choose authentication providers
        val providers = arrayListOf(
            /*AuthUI.IdpConfig.PhoneBuilder().build(),
            AuthUI.IdpConfig.GoogleBuilder().build(),*/
            AuthUI.IdpConfig.EmailBuilder().build())
        //create and launch sign-in intent
        return authUI.createSignInIntentBuilder()
            .setAvailableProviders(providers)
            .setLogo(R.drawable.ic_launcher_foreground)
            .setTheme(R.style.AppTheme)
            .build()
    }

    override fun saveUser() {
        val user = auth.currentUser
        if(user != null) database.saveUser(User(user.uid, user.safeDisplayName, user.detail))
    }
}