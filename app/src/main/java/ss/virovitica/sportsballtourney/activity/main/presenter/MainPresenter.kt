package ss.virovitica.sportsballtourney.activity.main.presenter

import android.util.Log
import androidx.annotation.StringRes
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.main.view.IMainView
import ss.virovitica.sportsballtourney.database.ILiveDatabase
import ss.virovitica.sportsballtourney.model.Tournament
import ss.virovitica.sportsballtourney.model.User
import ss.virovitica.sportsballtourney.utility.detail
import ss.virovitica.sportsballtourney.utility.safeDisplayName
import java.util.*

class MainPresenter(private val view: IMainView, private val database: ILiveDatabase, private val auth: FirebaseAuth):
    IMainPresenter, ILiveDatabase.Listener {

    //members
    private var tournament: Tournament? = null
    private val databaseListeners: MutableList<ValueEventListener> = LinkedList()
    private var deleting = false

    //functions

    //***IMainPresenter***//

    override fun setTournament(tournamentID: String?) {
        if(tournamentID != null) database.getTournament(this, tournamentID)
        else dataError(R.string.no_tournament)
    }

    override fun getUserDetails() {
        val user = auth.currentUser
        if(user == null) view.setUserDetails(User())
        else view.setUserDetails(User(user.uid, user.safeDisplayName, user.detail))
    }

    override fun getTournament() = tournament

    override fun unsubscribe() {
        for(listener in databaseListeners) {
            database.removeListener(listener)
        }
        databaseListeners.clear()
    }

    override fun notifyOfDelete() {
        deleting = true
    }

    //***ILiveDatabase.Listener***//

    override fun tournamentUpdate(tournament: Tournament, listener: ValueEventListener) {
        val firstTime = (this.tournament == null)
        this.tournament = tournament
        if(!databaseListeners.contains(listener)) databaseListeners.add(listener)
        view.setToolbarTitle(tournament.name)
        if(firstTime) view.goToFirstTab()
    }

    override fun dataError(@StringRes messageID: Int) {
        if(deleting) {
            deleting = false
            return
        }
        Log.i("MainPresenter", "dataError called")
        view.ctx.toast(messageID)
        view.back()
    }
}