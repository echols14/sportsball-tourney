package ss.virovitica.sportsballtourney.activity.editteams.view

import ss.virovitica.sportsballtourney.model.Team
import ss.virovitica.sportsballtourney.utility.`interface`.IView

interface IEditTeamsView: IView {
    fun setTitle(title: String)
    fun setTeams(teams: List<Team>)
    fun editDialog(team: Team)
}