package ss.virovitica.sportsballtourney.activity.main.presenter

import ss.virovitica.sportsballtourney.model.Tournament

interface IMainPresenter {
    fun setTournament(tournamentID: String?)
    fun getUserDetails()
    fun getTournament(): Tournament?
    fun unsubscribe()
    fun notifyOfDelete()
}