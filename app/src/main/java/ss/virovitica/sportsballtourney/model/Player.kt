package ss.virovitica.sportsballtourney.model

import java.util.*

data class Player(var name: String) {
    constructor(name: String, number: Int?): this(name) {
        this.number = number
    }
    constructor(): this("PLAYER NAME ERROR")

    //members
    val id: String = UUID.randomUUID().toString()
    var number: Int? = null
    val numberText: String
        get() =
            if(number == null) ""
            else number.toString()
    val goalValues: MutableList<Int> = LinkedList()

    //functions
    fun getTotalGoals() = goalValues.size
    fun getTotalPoints() = goalValues.sum()
    fun getAveragePointsPerGoal() =
        if(getTotalGoals() == 0) 0.0
        else (getTotalPoints().toDouble()) / (getTotalGoals().toDouble())

    //generated functions
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Player
        if (id != other.id) return false
        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }
}