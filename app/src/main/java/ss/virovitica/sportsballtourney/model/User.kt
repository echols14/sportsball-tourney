package ss.virovitica.sportsballtourney.model

data class User(val id: String = ANONYMOUS, val name: String = ANONYMOUS, val detail: String = "") {
    companion object {
        const val ANONYMOUS = "Anonymous"
    }

    //generated functions
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as User
        if (id != other.id) return false
        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }
}