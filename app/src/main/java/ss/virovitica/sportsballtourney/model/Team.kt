package ss.virovitica.sportsballtourney.model

import java.util.*
import kotlin.collections.ArrayList

data class Team(var name: String) {
    constructor(): this("TEAM NAME ERROR")

    companion object {
        const val MAX_TEAM_SIZE = 10
    }

    //members
    val id: String = UUID.randomUUID().toString()
    val playerIDs: MutableList<String> = ArrayList(MAX_TEAM_SIZE)
    val full: Boolean
        get() = (playerIDs.size == MAX_TEAM_SIZE)

    //functions

    fun addPlayer(player: Player): Boolean {
        if(playerIDs.size == MAX_TEAM_SIZE) return false
        return playerIDs.add(player.id)
    }

    //generated functions
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Team

        if (id != other.id) return false

        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }
}