package ss.virovitica.sportsballtourney.model

import java.util.*

data class Match(val teamIdA: String, val teamIdB: String) {
    constructor(): this("TEAM ID ERROR", "TEAM ID ERROR")

    //members
    val id: String = UUID.randomUUID().toString()
    private val goalsTeamA: MutableList<Goal> = LinkedList()
    private val goalsTeamB: MutableList<Goal> = LinkedList()
    val scoreTeamA: Int
        get() {
            var pointsSum = 0
            for(goal in goalsTeamA) pointsSum += goal.value
            return pointsSum
        }
    val scoreTeamB: Int
        get() {
            var pointsSum = 0
            for(goal in goalsTeamB) pointsSum += goal.value
            return pointsSum
        }
    var finished = false
        private set

    //functions

    fun pointTeamA(goal: Goal) {
        var insertionIndex = 0
        while(insertionIndex < goalsTeamA.size && goalsTeamA[insertionIndex].time < goal.time) insertionIndex++
        goalsTeamA.add(insertionIndex, goal)
    }
    fun pointTeamB(goal: Goal) {
        var insertionIndex = 0
        while(insertionIndex < goalsTeamB.size && goalsTeamB[insertionIndex].time < goal.time) insertionIndex++
        goalsTeamB.add(insertionIndex, goal)
    }

    fun finish() {
        finished = true
    }

    fun getGoalsTeamA(): List<Goal> = goalsTeamA
    fun getGoalsTeamB(): List<Goal> = goalsTeamB

    //generated functions
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Match
        if (id != other.id) return false
        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }
}