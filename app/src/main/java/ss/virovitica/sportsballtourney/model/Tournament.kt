package ss.virovitica.sportsballtourney.model

import java.util.*
import kotlin.collections.ArrayList

data class Tournament(val name: String) {
    constructor(): this("TOURNAMENT NAME ERROR")
    companion object {
        const val TOURNAMENT_SIZE = 16
        const val NUMBER_OF_GROUPS = 4
    }

    //members
    val id = UUID.randomUUID().toString()
    val teamIDs: MutableList<String> = ArrayList(TOURNAMENT_SIZE)
    private val bracketMatchIDs: MutableList<String> = ArrayList(TOURNAMENT_SIZE - 1)
    private val groups: MutableList<Group> = ArrayList(NUMBER_OF_GROUPS)
    val managers: MutableList<User> = LinkedList()
    var started = false
        private set
    var finished = false
        private set
    val ready: Boolean
        get() = (teamIDs.size == TOURNAMENT_SIZE)
    var bracketStarted = false
        private set

    //functions

    fun getBracketMatchIDs(): List<String> = bracketMatchIDs
    fun getGroups(): List<Group> = groups

    fun startTournament(): Boolean {
        if(!ready) return false
        started = true
        generateGroups()
        return true
    }

    fun finishTournament() {
        finished = true
    }

    fun startBracket() {
        bracketStarted = true
    }

    fun addTeam(teamID: String): Boolean {
        if(started || teamIDs.size >= TOURNAMENT_SIZE) return false
        if(teamIDs.contains(teamID)) return false
        teamIDs.add(teamID)
        return true
    }

    fun addMatches(matches: List<Match>) {
        for(match in matches) {
            bracketMatchIDs.add(match.id)
        }
    }

    fun addManager(managerID: String, managerName: String, detail: String): Boolean {
        for (user in managers) {
            if (user.id == managerID) return false
        }
        managers.add(User(managerID, managerName, detail))
        return true
    }

    private fun generateGroups() {
        //create a shuffled copy of the list of teamIDs so the grouping is random
        val shuffledList: MutableList<String> = ArrayList(teamIDs.size)
        shuffledList.addAll(teamIDs)
        shuffledList.shuffle()
        //create groups from the shuffled list
        for(groupNumber in 1..NUMBER_OF_GROUPS) {
            val newGroup = Group(groupNumber)
            for(i in 0 until Group.GROUP_SIZE) {
                val indexInList = ((groupNumber - 1) * Group.GROUP_SIZE) + i
                newGroup.addTeam(shuffledList[indexInList])
            }
            groups.add(newGroup)
        }
    }

    //generated functions
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Tournament
        if (id != other.id) return false
        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }
}