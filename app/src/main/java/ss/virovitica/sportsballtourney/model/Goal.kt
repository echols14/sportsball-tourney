package ss.virovitica.sportsballtourney.model

data class Goal(val time: Int, val scoringPlayerID: String, val value: Int) {
    constructor(): this(0, "PLAYER ID ERROR", 0)

    var scoringPlayerName: String = ""
}