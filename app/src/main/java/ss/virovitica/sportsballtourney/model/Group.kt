package ss.virovitica.sportsballtourney.model

import java.util.*
import kotlin.collections.ArrayList

data class Group(val groupNumber: Int) {
    constructor(): this(-1)

    companion object {
        const val GROUP_SIZE = 4
        /**this value should be the value of Combination(GROUP_SIZE, 2),
         * where Combination(n,r) is the mathematical function commonly notated as nCr**/
        const val NUMBER_OF_MATCHES = 6
    }

    //members
    val id = UUID.randomUUID().toString()
    private val teamIDs: MutableList<String> = ArrayList(GROUP_SIZE)
    private val matchIDs: MutableList<String> = ArrayList(NUMBER_OF_MATCHES)
    var finished = false
        private set

    //functions

    fun addTeam(teamID: String): Boolean {
        if(teamIDs.size == GROUP_SIZE || teamIDs.contains(teamID)) return false
        teamIDs.add(teamID)
        return true
    }

    fun addMatch(matchID: String): Boolean {
        if(matchIDs.size == NUMBER_OF_MATCHES || matchIDs.contains(matchID)) return false
        matchIDs.add(matchID)
        return true
    }

    fun getTeamIDs(): List<String> = teamIDs
    fun getMatchIDs(): List<String> = matchIDs

    fun finish() { finished = true }

    //generated functions
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Group
        if (id != other.id) return false
        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }
}