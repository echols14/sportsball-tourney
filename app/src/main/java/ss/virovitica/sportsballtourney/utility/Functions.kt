package ss.virovitica.sportsballtourney.utility

import android.content.res.Resources
import android.util.Log
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ss.virovitica.sportsballtourney.model.Group
import ss.virovitica.sportsballtourney.model.Match
import ss.virovitica.sportsballtourney.model.Tournament
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun getScreenHeight() = Resources.getSystem().displayMetrics.heightPixels

fun switchToFragment(fm: FragmentManager, @IdRes fragmentDestinationID: Int, newFragment: Fragment) {
    val currentFragment = fm.findFragmentById(fragmentDestinationID)
    if(currentFragment != null) fm.beginTransaction().remove(currentFragment).commit()
    fm.beginTransaction().add(fragmentDestinationID, newFragment).commit()
}

fun generateFirstLevelOfBracket(tournament: Tournament, matches: Map<String, Match>): List<Match> {
    val (standingsMap, tieMap) = generateGroupStandings(tournament, matches)
    val criticalTieGroupIndex = criticalTieGroup(standingsMap, tieMap)
    if(criticalTieGroupIndex != null) {
        Log.e("Generate Bracket", "CRITICAL TIE UNHANDLED; critical group: $criticalTieGroupIndex")
    }
    val newMatches: MutableList<Match> = ArrayList(standingsMap.size * 2)
    for (i in 1..standingsMap.size step 2) {
        val standingsA = standingsMap.getValue(i)
        val standingsB = standingsMap.getValue(i + 1)
        newMatches.add(Match(standingsA[0], standingsB[1]))
        newMatches.add(Match(standingsA[1], standingsB[0]))
    }
    return newMatches
}

fun criticalTieGroup(standingsMap: Map<Int, List<String>>, tieMap: Map<Int, List<String>?>): Int? {
    for (groupNumber in 1..standingsMap.size) {
        val standingsList = standingsMap.getValue(groupNumber)
        val tieList = tieMap[groupNumber]
        if(tieList.isNullOrEmpty()) continue
        for((i, teamID1) in standingsList.withIndex()) {
            if(tieList.contains(teamID1)) { //part of a tie
                for((j, teamID2) in standingsList.withIndex()) {
                    if(i == j) continue
                    if(tieList.contains(teamID2)) { //also part of a tie
                        if((i == 1 && j == 2) || (i == 2 && j == 1)) return groupNumber
                    }
                }
            }
        }
    }
    return null
}

fun generateNextBracketLevel(matches: List<Match>): List<Match> {
    if(matches.size % 2 == 1) return ArrayList() //error protection
    //find the winners of the old matches
    val winningTeams: MutableList<String> = ArrayList(matches.size)
    for(match in matches) {
        if(match.scoreTeamA > match.scoreTeamB) winningTeams.add(match.teamIdA)
        else winningTeams.add(match.teamIdB)
        //there should never be a tie in bracket play! (but if there somehow is, team B wins...)
    }
    //pair up the winners into new matches
    val newMatches: MutableList<Match> = ArrayList(matches.size / 2)
    for(i in 0 until winningTeams.size step 2) {
        val teamA = winningTeams[i]
        val teamB = winningTeams[i+1]
        newMatches.add(Match(teamA, teamB))
    }
    return newMatches
}

fun countMetaPoints(group: Group, matches: Map<String, Match>): HashMap<String, Int> {
    val tiePoints = 1
    val winPoints = 3
    //initialize a place to store meta-point totals for each team
    val metaPointsMap = HashMap<String, Int>()
    for(teamID in group.getTeamIDs()) {
        metaPointsMap[teamID] = 0
    }
    //count points
    for(matchID in group.getMatchIDs()) {
        val match = matches[matchID]
        if (match != null && match.finished) { //ready to be counted
            val scoreA = match.scoreTeamA
            val scoreB = match.scoreTeamB
            //count meta-points
            if (scoreA == scoreB) {
                val oldPointsA = metaPointsMap[match.teamIdA]
                if (oldPointsA != null) metaPointsMap[match.teamIdA] = oldPointsA + tiePoints
                val oldPointsB = metaPointsMap[match.teamIdB]
                if (oldPointsB != null) metaPointsMap[match.teamIdB] = oldPointsB + tiePoints
            } else if (scoreA > scoreB) {
                val oldPointsA = metaPointsMap[match.teamIdA]
                if (oldPointsA != null) metaPointsMap[match.teamIdA] = oldPointsA + winPoints
            } else { //scoreA < scoreB
                val oldPointsB = metaPointsMap[match.teamIdB]
                if (oldPointsB != null) metaPointsMap[match.teamIdB] = oldPointsB + winPoints
            }
        }
    }
    return metaPointsMap
}

fun countDifferencePoints(group: Group, matches: Map<String, Match>): HashMap<String, Int> {
    //initialize a place to store difference point totals for each team
    val differencePointsMap = HashMap<String, Int>()
    for(teamID in group.getTeamIDs()) {
        differencePointsMap[teamID] = 0
    }
    //count points
    for(matchID in group.getMatchIDs()) {
        val match = matches[matchID]
        if(match != null && match.finished) { //ready to be counted
            val scoreA = match.scoreTeamA
            val scoreB = match.scoreTeamB
            //count difference points
            val oldTotalA = differencePointsMap[match.teamIdA]
            if(oldTotalA != null) differencePointsMap[match.teamIdA] = oldTotalA + scoreA - scoreB
            val oldTotalB = differencePointsMap[match.teamIdB]
            if(oldTotalB != null) differencePointsMap[match.teamIdB] = oldTotalB + scoreB - scoreA
        }
    }
    return differencePointsMap
}

fun countTotalPoints(group: Group, matches: Map<String, Match>): HashMap<String, Int> {
    //initialize a place to store point totals for each team
    val totalPointsMap = HashMap<String, Int>()
    for(teamID in group.getTeamIDs()) {
        totalPointsMap[teamID] = 0
    }
    //count points
    for(matchID in group.getMatchIDs()) {
        val match = matches[matchID]
        if(match != null && match.finished) { //ready to be counted
            val scoreA = match.scoreTeamA
            val scoreB = match.scoreTeamB
            //count total points
            val oldTotalA = totalPointsMap[match.teamIdA]
            if(oldTotalA != null) totalPointsMap[match.teamIdA] = oldTotalA + scoreA
            val oldTotalB = totalPointsMap[match.teamIdB]
            if(oldTotalB != null) totalPointsMap[match.teamIdB] = oldTotalB + scoreB
        }
    }
    return totalPointsMap
}

data class StandingsResult(val standingsMap: Map<Int, List<String>>, val tieMap: Map<Int, List<String>?>)

fun generateGroupStandings(tournament: Tournament, matches: Map<String, Match>): StandingsResult {
    val tieMap: MutableMap<Int, List<String>> = HashMap()
    val groupStandings: MutableMap<Int, List<String>> = HashMap()
    for(group in tournament.getGroups()) {
        //count points
        val metaPointsMap = countMetaPoints(group, matches)
        //sort the group's teams according to standing
        val standingsList = ArrayList<String>(metaPointsMap.size)
        while(metaPointsMap.isNotEmpty()) {
            val highestMetaScore = metaPointsMap.values.max()
            val subList = LinkedList<String>()
            //copy the map for iteration purposes
            val metaPointsMapCopy = HashMap<String, Int>()
            metaPointsMapCopy.putAll(metaPointsMap)
            for(pair in metaPointsMapCopy) {
                if(pair.value == highestMetaScore) {
                    //move the teamID from the map of scores to the sub-list
                    subList.add(pair.key)
                    metaPointsMap.remove(pair.key)
                }
            }
            //check for ties in meta-points
            if(subList.size <= 1) standingsList.addAll(subList) //no tie
            else { //yes tie
                //extract the total point differences for the teams that are tied by meta-points
                val differencePointsMiniMap: MutableMap<String, Int> = HashMap()
                val differencePointsMap = countDifferencePoints(group, matches)
                for(id in subList) differencePointsMiniMap[id] = differencePointsMap[id] ?: 0
                //sort out the teams based on total point differences
                while(differencePointsMiniMap.isNotEmpty()) {
                    val highestDifferenceScore = differencePointsMiniMap.values.max()
                    val subList2 = LinkedList<String>()
                    //copy the map for iteration purposes
                    val differencePointsMiniMapCopy = HashMap<String, Int>()
                    differencePointsMiniMapCopy.putAll(differencePointsMiniMap)
                    for(pair in differencePointsMiniMapCopy) {
                        if(pair.value == highestDifferenceScore) {
                            //move the teamID from the map of scores to the sub-list
                            subList2.add(pair.key)
                            differencePointsMiniMap.remove(pair.key)
                        }
                    }
                    //check for ties in difference-points
                    if(subList2.size <= 1) standingsList.addAll(subList2) //no tie
                    else { //yes tie
                        //extract the total point differences for the teams that are tied by meta-points
                        val totalPointsMiniMap: MutableMap<String, Int> = HashMap()
                        val totalPointsMap = countTotalPoints(group, matches)
                        for(id in subList2) totalPointsMiniMap[id] = totalPointsMap[id] ?: 0
                        //sort out the teams based on total point sums
                        while(totalPointsMiniMap.isNotEmpty()) {
                            val highestTotalScore = totalPointsMiniMap.values.max()
                            //copy the map for iteration purposes
                            val totalPointsMiniMapCopy = HashMap<String, Int>()
                            totalPointsMiniMapCopy.putAll(totalPointsMiniMap)
                            val subList3 = LinkedList<String>()
                            for (pair in totalPointsMiniMapCopy) {
                                //if teams are still tied, their order in the map is taken as an arbitrary tie-breaker
                                if (pair.value == highestTotalScore) {
                                    //move the teamID from the map of scores to the sub-list
                                    subList3.add(pair.key)
                                    totalPointsMiniMap.remove(pair.key)
                                }
                            }
                            if(subList3.size <= 1) standingsList.addAll(subList3) //no tie
                            else { //WE STILL HAVE A TIE GOSH DARN YOU
                                standingsList.addAll(subList3)
                                tieMap[group.groupNumber] = subList3
                            }
                        }
                    }
                }
            }
        }
        //save the final result
        groupStandings[group.groupNumber] = standingsList
    }
    return StandingsResult(groupStandings, tieMap)
}