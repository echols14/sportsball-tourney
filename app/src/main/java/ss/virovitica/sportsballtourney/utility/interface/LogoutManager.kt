package ss.virovitica.sportsballtourney.utility.`interface`

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.activity.login.view.LoginActivity
import ss.virovitica.sportsballtourney.dialog.LogoutDialog

interface LogoutManager: LogoutDialog.Listener {
    //members
    val context: Context
    val fragmentManager: FragmentManager
    val authUI: AuthUI
    val auth: FirebaseAuth

    //functions

    fun attemptLogout() {
        if(auth.currentUser != null) LogoutDialog(this).show(fragmentManager, LogoutDialog.LOGOUT_DIALOG)
        else executeLogout()
    }

    private fun executeLogout() {
        authUI.signOut(context).addOnCompleteListener {
            context.startActivity(context.intentFor<LoginActivity>().clearTop())
            context.toast(R.string.logged_out)
        }
    }

    //***LogoutDialog.Listener***//

    override fun onLogout() { executeLogout() }
}