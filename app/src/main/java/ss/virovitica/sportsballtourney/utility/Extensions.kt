package ss.virovitica.sportsballtourney.utility

import android.content.Context
import android.view.View
import com.google.firebase.auth.FirebaseUser
import ss.virovitica.sportsballtourney.model.User.Companion.ANONYMOUS

val View.ctx: Context
    get() = context

val FirebaseUser.detail: String
    get() =
        if(providerData.size == 0) ""
        else {
            val email = providerData[0].email
            email ?: ""
        }

val FirebaseUser.safeDisplayName: String
    get() = displayName ?: ANONYMOUS