package ss.virovitica.sportsballtourney.utility.`interface`

import android.util.Log
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.Toolbar
import org.jetbrains.anko.toast
import ss.virovitica.sportsballtourney.R
import ss.virovitica.sportsballtourney.utility.ctx

interface ToolbarManager {
    //members
    val toolbar: Toolbar
    var toolbarTitle: String
        get() = toolbar.title.toString()
        set(value){
            toolbar.title = value
        }


    //functions

    private fun createUpDrawable() = DrawerArrowDrawable(toolbar.ctx).apply { progress = 1f }

    fun enableHomeAsUp(up: () -> Unit) {
        toolbar.navigationIcon = createUpDrawable()
        toolbar.setNavigationOnClickListener { up() }
    }

    fun initToolbarTournaments(loggedIn: Boolean){
        if(loggedIn) { toolbar.inflateMenu(R.menu.tournaments_toolbar) }
        else { toolbar.inflateMenu(R.menu.tournaments_toolbar_guest) }
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_logout -> logout()
                R.id.action_new_tournament -> newTournament()
                else -> toolbar.ctx.toast(R.string.unknown_option)
            }
            true
        }
    }

    fun initToolbarEditTeams(){
        toolbar.inflateMenu(R.menu.edit_teams_toolbar)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_new_team -> newTeam()
                else -> toolbar.ctx.toast(R.string.unknown_option)
            }
            true
        }
    }

    fun initToolbarEditPlayers(){
        toolbar.inflateMenu(R.menu.edit_players_toolbar)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_new_player -> newPlayer()
                else -> toolbar.ctx.toast(R.string.unknown_option)
            }
            true
        }
    }

    fun logout() {
        toolbar.ctx.toast(R.string.unknown_option)
        Log.e("ToolbarManager", "logout() function not implemented, but called")
    }

    fun newTournament() {
        toolbar.ctx.toast(R.string.unknown_option)
        Log.e("ToolbarManager", "newTournament() function not implemented, but called")
    }

    fun newTeam() {
        toolbar.ctx.toast(R.string.unknown_option)
        Log.e("ToolbarManager", "newTeam() function not implemented, but called")
    }

    fun newPlayer() {
        toolbar.ctx.toast(R.string.unknown_option)
        Log.e("ToolbarManager", "newPlayer() function not implemented, but called")
    }
}