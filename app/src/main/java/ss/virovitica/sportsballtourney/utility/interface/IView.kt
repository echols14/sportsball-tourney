package ss.virovitica.sportsballtourney.utility.`interface`

import android.content.Context

interface IView {
    val ctx: Context
}